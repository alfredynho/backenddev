<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="FriendBot, Academico" />
<meta name="description" content="FriendBot"/>
<meta name="author" content="friendBot" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="theme-color" content="#E2211C">

<meta name="robots" content="index,follow" />
<link rel="canonical" href="https://alfredynho.github.io/" />
<link rel="publisher" href="{{ getSocial("PAGINA_WEB") }}"/>

<meta property="og:locale" content="es_LA" />
<meta property="og:type" content="website" />
<meta property="og:title" content="FriendBot" />
<meta property="og:description" content="" />
<meta property="og:url" content="{{ getSocial("PAGINA_WEB") }}" />
<meta property="og:site_name" content="FriendtBot" />
<meta property="og:image" content="https://alfredynho.github.io/">

<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="FriendBot"/>
<meta name="twitter:title" content="FriendBot"/>
<meta name="twitter:site" content="@FriendBot"/>
<meta name="twitter:creator" content="@FriendBot"/>

 <title>@yield('title')</title>

<link rel="shortcut icon" href="{{ asset('frontend/images/favicon.webp') }}" />

@include('frontend.partials.css')

@yield('extracss')

@show

</head>

<body>

<div class="wrapper">

<header id="header" class="header default" style="background-color: #1A1A1A;">
    {{--  @include('frontend.partials.header')  --}}


<div class="menu">
   <nav id="menu" class="mega-menu">
        @include('frontend.partials.nav')
   </nav>

 </div>
</header>

    @yield('content')

<footer class="footer page-section-pt black-bg">
    @include('frontend.partials.footer')
</footer>

</div>

<div id="back-to-top">
    <a class="top arrow" href="#top">
        <span><br>
            <i class="fa fa-rocket"></i>
        </span>
    </a>
</div>

    @include('frontend.partials.js')

    <script src="{{ asset('frontend/js/pushbar.js') }}"></script>

    <script type="text/javascript">
        var pushbar = new Pushbar({
            blur:true,
            overlay:true
        });

        var interval = setTimeout(function(){
            pushbar.open('pushbar-notification');
        },100);
    </script>

@yield('extrajs')

</body>

</html>

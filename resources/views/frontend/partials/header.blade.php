  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 xs-mb-10">
        <div class="topbar-call text-center text-md-left">
          <ul>
            <li><i class="fa fa-globe theme-color"></i> <a href="{{ getSocial("PAGINA_WEB") }}" target="_blank" ><span> www.friendbot.ml </span></a></li>
             {{--  <li><i class="fa fa-whatsapp"></i> <a href="https://wa.me/70520769?text=Me%20gustaría%20saber%20mas%20de%20los%20eventos" target="_blank"> <span> 70520769 </span> </a> </li>  --}}
          </ul>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="topbar-social text-center text-md-right">
          <ul>
            <li><a href="{{ getSocial("FACEBOOK") }}" target="_blank"><span class="ti-facebook"></span></a></li>
            <li><a href="{{ getSocial("INSTAGRAM") }}" target="_blank"><span class="ti-instagram"></span></a></li>
            <li><a href="{{ getSocial("WHASAPP_LA_PAZ") }}" target="_blank"><span class="fa fa-whatsapp"></span></a></li>
            <li><a href="{{ getSocial("LINKEDIN") }}" target="_blank"><span class="ti-linkedin"></span></a></li>
            <li><a href="{{ getSocial("YOUTUBE") }}" target="_blank"><span class="ti-youtube"></span></a></li>
          </ul>
        </div>
      </div>
     </div>
  </div>

    <section class="menu-list-items">
     <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <ul class="menu-logo">
            <li>
                <a href="{{ route('index') }}"><img id="logo_img" src="{{ asset('frontend/images/friendbotlogo.png') }}" alt="FriendBot"> </a>
            </li>
        </ul>
        <div class="menu-bar">
         <ul class="menu-links">
         <li><a href="#">Inicio</a></li>
            <li><a href="#"> Quienes Somos</a></li>
            <li><a >Eventos <i class="fa fa-angle-down fa-indicator"></i></a>
                <ul class="drop-down-multilevel">
                    <li><a href="#">Calendario de eventos </a></li>
                    <li><a href="#">Programar una cita </a></li>
                </ul>
            </li>

            <li><a href="#">Blog</a></li>
            <li><a href="#">Contactos</a></li>
        </ul>
        </div>
       </div>
      </div>
     </div>
    </section>

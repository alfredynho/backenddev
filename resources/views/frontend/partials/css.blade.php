<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">

<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/plugins-css.css') }}" />

<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/typography.css') }}" />

<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/shortcodes/shortcodes.css') }}" />

<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css') }}" />

<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/responsive.css') }}" />

<style>
    /* style social buttons */
    .social-bar {
        position: fixed;
        right: 0;
        top: 35%;
        font-size: 1.5rem;
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        z-index: 100;
    }

</style>

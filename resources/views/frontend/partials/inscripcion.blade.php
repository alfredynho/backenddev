<div class="col-lg-12 col-md-12">
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="exampleModalLongTitle">
                <div class="section-title mb-10">
                <h6>FORMULARIO DE INSCRIPCIÓN</h6>
                <h6>(Recibiras un correo electrónico de confirmación a nuestro evento en 48 horas)</h6>

            {!! Form::open(['route' => 'dashboard.inscription.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}
                <div class="contact-form clearfix">

                <div class="section-field">
                    <input id="firstname" name="firstname" type="text" placeholder="Nombres*" class="form-control" name="nombre">
                </div>

                <div class="section-field">
                    <input id="lastname" name="lastname" type="text" placeholder="Apellidos*" class="form-control" name="apellidos">
                </div>

                <div class="section-field">
                    <input id="email" name="email" type="email" placeholder="Email*" class="form-control" name="email">
                </div>

                <div class="section-field">
                    <input id="compania" name="compania" type="text" placeholder="Compañia*" class="form-control" name="compañia">
                </div>

                <div class="section-field">
                    <input id="rubro" type="text" placeholder="rubro*" class="form-control" name="rubro">
                </div>

                <div class="form-group">
                    <select id="evento" name="evento" class="form-control" required>
                        @foreach ($events as $eve)
                            <option value="{{$eve->id}}">{{ $eve->title }} </option>
                        @endforeach
                    </select>
                </div>

                </div>

                </div>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="button"><span> Enviar</span></button>
            </div>
            </div>

            {{Form::Close()}}

        </div>
    </div>
</div>

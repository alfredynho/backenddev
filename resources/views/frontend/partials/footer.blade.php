 <div class="container">
  <div class="row">
      <div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Navegación</h6>
        <ul>
          <li><a href="#">Inicio</a></li>
          <li><a href="#">Quienes somos</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Contactos</a></li>
        </ul>
      </div>
    </div>

    <div class="col-lg-4 col-sm-6 xs-mb-30">
    <h6 class="text-white mb-30 mt-10 text-uppercase">Contactos</h6>
    <ul class="addresss-info">
        <li><i class="fa fa-map-marker"></i> <p> <span style="color:#4680FF; font-weight: bold;"> La Paz - Bolivia </span>: Dirección Zona Villa Antonio Calle San Alberto 8A </p> </li>
        <li><i class="fa fa-phone"></i> <a href="tel:70520769"> <span>Telf: 73053045 - 71978309 | Whatsapp: 73053045 - 71978309 </span> </a> </li>
        <li><i class="fa fa-envelope-o"></i>Email:friendbotall@gmail.com</li>
      </ul>
    </div>

    <div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase"></h6>
        <ul>
          <li><a href="#"></a></li>
          <li><a href="#"></a></li>
          <li><a href="#"></a></li>
          <li><a href="#"></a></li>
          <li><a href="#"></a></li>
        </ul>
      </div>
    </div>

    <div class="col-lg-4 col-sm-6">
        <p class="mb-30">Mantente informado de todos nuestros eventos, productos, descuentos y mas!</p>
         <div class="footer-Newsletter">
          <div>

            {{-- {!! Form::open(['route' => 'dashboard.suscription.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!} --}}
             <div id="msg"> </div>
              <div>
                <input id="email" name="email" class="form-control" type="text" placeholder="ingresa tu correo electronico" name="email1" value="">
              </div>
              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                    <input type="text" name="b_b7ef45306f8b17781aa5ae58a_6b09f39a55" tabindex="-1" value="">
                </div>
                <div class="clear">
                  <button type="submit" name="submitbtn" id="ajaxSubmit" class="button button-border mt-20 form-button">  Suscribirme </button>
                </div>
            {{-- {{Form::Close()}} --}}

            </div>
            </div>
         </div>
       </div>
      <div class="footer-widget mt-20">
        <div class="row">
          <div class="col-lg-6 col-md-6">
           <p class="mt-15"> &copy; {{ now()->year }} <a href="{{ getSocial("PAGINA_WEB") }}" target="_blank"> Amper </a> Todos los derechos reservados </p>
          </div>
          <div class="col-lg-6 col-md-6 text-left text-md-right">
            <div class="social-icons color-hover mt-10">
             <ul>
              <li class="social-facebook"><a href="{{ getSocial("FACEBOOK") }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li class="social-linkedin"><a href="{{ getSocial("WHASAPP_LA_PAZ") }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
              <li class="social-dribbble"><a href="{{ getSocial("YOUTUBE") }}" target="_blank"><i class="fa fa-youtube"></i> </a></li>
              <li class="social-instagram"><a href="{{ getSocial("INSTAGRAM") }}" target="_blank"><i class="fa fa-instagram"></i> </a></li>
              <li class="social-dropbox"><a href="{{ getSocial("LINKEDIN") }}" target="_blank"><i class="fa fa-linkedin"></i> </a></li>
             </ul>
           </div>
          </div>
        </div>
      </div>
  </div>

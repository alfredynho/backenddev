@extends('frontend.layout')

@section('title', __('FriendBot'))

@section('extracss')
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/leaflet.css') }}" />

    <style>
        #mapid { height: 300px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#1A1A1A;
            color: #ffffff;
        }
    </style>

    <style>
    .pushbar {
        background:#FFFFFF;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#333333;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    </style>

    <link rel="stylesheet" href="{{ asset('frontend/css/pushbar.css') }}">

@endsection('extracss')

@section('content')

<section class="rev-slider">
  <div id="rev_slider_269_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-3" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
  <div id="rev_slider_269_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
<ul>  <!-- SLIDE  -->
    <li data-index="rs-759" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
        <img src="{{ asset('frontend/images/sliderAX.webp')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
    <!-- LAYERS -->

    <!-- LAYER NR. 1 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-3"
       id="slide-759-layer-22"
       data-x="-248"
       data-y="505"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 5;"><img src="{{ asset('frontend/images/slider-03/paper.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 2 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-23"
       data-x="right" data-hoffset="-180"
       data-y="-170"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 6;"><img src="{{ asset('frontend/images/slider-03/ea23f-8.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 3 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-24"
       data-x="right" data-hoffset="-200"
       data-y="center" data-voffset="85"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 7;"><img src="{{ asset('frontend/images/slider-03/ea23f-8.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 4 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-25"
       data-x="-299"
       data-y="-169"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 8;"><img src="{{ asset('frontend/images/slider-03/756d0-9.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 5 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-6"
       data-x="160"
       data-y="bottom" data-voffset="370"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 9;">

<div class="rs-looped rs-slideloop"  data-easing="" data-speed="3" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/756d0-9.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 6 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-7"
       data-x="right" data-hoffset="540"
       data-y="bottom" data-voffset="135"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 10;"><img src="{{  asset('frontend/images/slider-03/ab657-13.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 7 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-8"
       data-x="center" data-hoffset="10"
       data-y="center" data-voffset=""
            data-width="['auto']"
      data-height="['auto']"

            data-type="text"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 11; white-space: nowrap; font-size: 330px; line-height: 242px; font-weight: 600; color: #ffffff; letter-spacing: 20px;font-family:Dosis;">
 </div>

    <!-- LAYER NR. 8 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-9"
       data-x="center" data-hoffset="-200"
       data-y="center" data-voffset="-48"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 12;"><img src="{{ asset('frontend/images/laptop.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 9 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-10"
       data-x="center" data-hoffset="-75"
       data-y="center" data-voffset="58"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 13;"><img src="{{ asset('frontend/images/slider-03/7f737-11.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 10 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-11"
       data-x="center" data-hoffset="-320"
       data-y="235"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 14;">
<div class="rs-looped rs-slideloop"  data-easing="" data-speed="3" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/db7df-10.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 11 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-12"
       data-x="190"
       data-y="bottom" data-voffset="155"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 15;">
<div class="rs-looped rs-slideloop"  data-easing="Bounce.easeIn" data-speed="5" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/7c087-6.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 12 -->
    <div class="tp-caption   tp-resizeme"
       id="slide-759-layer-13"
       data-x="center" data-hoffset="-60"
       data-y="bottom" data-voffset="60"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 16;"><img src="{{ asset('frontend/slider-03/5536f-Shadow.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div>

    <!-- LAYER NR. 13 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-14"
       data-x="center" data-hoffset="400"
       data-y="180"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 17;">
<div class="rs-looped rs-slideloop"  data-easing="" data-speed="3" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/85cc7-5.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 14 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-3"
       id="slide-759-layer-15"
       data-x="right" data-hoffset="290"
       data-y="140"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 18;">
<div class="rs-looped rs-slideloop"  data-easing="Bounce.easeInOut" data-speed="4" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/2a3ef-4.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 15 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-16"
       data-x="right" data-hoffset="500"
       data-y="bottom" data-voffset="50"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 19;">
<div class="rs-looped rs-slideloop"  data-easing="" data-speed="3" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/2f5f7-7.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 16 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
       id="slide-759-layer-17"
       data-x="right" data-hoffset="400"
       data-y="bottom" data-voffset="30"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 20;">
<div class="rs-looped rs-slideloop"  data-easing="Linear.easeNone" data-speed="2" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/ea23f-8.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>

    <!-- LAYER NR. 17 -->
    <div class="tp-caption   tp-resizeme"
       id="slide-759-layer-18"
       data-x="center" data-hoffset="410"
       data-y="center" data-voffset="170"
            data-width="['auto']"
      data-height="['auto']"

            data-type="text"
      data-responsive_offset="on"

            data-frames='[{"delay":370,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 21; white-space: nowrap; font-size: 40px; line-height: 44px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;"></div>

    <!-- LAYER NR. 18 -->
    <a class="tp-caption rev-btn  tp-resizeme"
 href="#" target="_self"       id="slide-759-layer-20"
       data-x="center" data-hoffset="490"
       data-y="bottom" data-voffset="199"
            data-width="['auto']"
      data-height="['auto']"

            data-type="button"
      data-actions=''
      data-responsive_offset="on"

            data-frames='[{"delay":840,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"600","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[12,12,12,12]"
            data-paddingright="[35,35,35,35]"
            data-paddingbottom="[12,12,12,12]"
            data-paddingleft="[35,35,35,35]"

            style="z-index: 22; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat;text-transform:uppercase;background-color:rgb(0,0,0);border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"></a>

    <!-- LAYER NR. 19 -->
    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
       id="slide-759-layer-26"
       data-x="right" data-hoffset="1475"
       data-y="bottom" data-voffset="640"
            data-width="['none','none','none','none']"
      data-height="['none','none','none','none']"

            data-type="image"
      data-responsive_offset="on"

            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 23;">
<div class="rs-looped rs-slideloop"  data-easing="" data-speed="3" data-xs="0" data-xe="10" data-ys="0" data-ye="10"><img src="{{ asset('frontend/images/slider-03/2f5f7-7.png')}}" alt="" data-ww="auto" data-hh="auto" data-no-retina> </div></div>
  </li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
</div>
</section>


<section class="page-section-ptb position-relative">
   <div class="container">
   <div class="row">
      <div class="col-md-8">
        <div class="section-title">
          <h6>FriendBot</h6>
          <h2 class="title-effect">Nuestros Servicios</h2>
        </div>
      </div>
    </div>
     <div class="row">
        <div class="col-md-4 xs-mb-40">
          <div class="feature-box h-100">
            <div class="feature-box-content">
            <i class="fa fa-users blue"></i>
            <h4>Usuarios</h4>
            <p>Mil millones de usuarios activos mensuales en todo el mundo</p>
            </div>
            <a href="#">Ver mas</a>
            <div class="feature-box-img" style="background-image: url({{ asset('frontend/images/friendbot.png')}});"></div>
            <span class="feature-border"></span>
          </div>
        </div>
        <div class="col-md-4 xs-mb-40">
          <div class="feature-box h-100">
            <div class="feature-box-content">
            <i class="fa fa-paper-plane"></i>
            <h4>Información oportuna</h4>
            <p>Donde lo necesites, cuando lo necesites , obten respuestas a tus dudas al instante</p>
            </div>
            <a href="#">Ver mas</a>
            <div class="feature-box-img" style="background-image: url({{ asset('frontend/images/friendbot.png')}});"></div>
            <span class="feature-border"></span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="feature-box h-100">
            <div class="feature-box-content">
            <i class="fa fa-reddit-alien"></i>
            <h4>Atención 24/7</h4>
            <p>Nuestro Asistente Virtual se encuentra disponible para la atencion en 24/7</p>
            </div>
            <a href="#">Ver mas</a>
            <div class="feature-box-img" style="background-image: url({{ asset('frontend/images/friendbot.png')}});"></div>
            <span class="feature-border"></span>
          </div>
        </div>
       </div>
   </div>
</section>

@include('frontend.partials.inscripcion')

<section class="action-box theme-bg full-width">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="action-box-text">
        <h3><strong> FriendBot: </strong>  Gracias a la plataforma se mejorará el tiempo de respuesta para la atención al cliente. </h3>
        <p>Asistente Virtual 24/7.</p>
      </div>
      <div class="action-box-button">
        <a class="button button-border white" href="#">
          <span>Chatear</span>
          <i class="fa fa-download"></i>
       </a>
     </div>
    </div>
  </div>
 </div>
</section>

<section class="page-section-ptb">
  <div class="container">

   <div class="row mt-80">
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="70" data-delay="0" data-type="%">
            <div class="skill-title">CSS3</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="75" data-delay="0" data-type="%">
            <div class="skill-title">PHP</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="80" data-delay="0" data-type="%">
            <div class="skill-title">JavaScript</div>
        </div>
     </div>
     </div>
     </div>

   <div class="row mt-80">
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="95" data-delay="0" data-type="%">
            <div class="skill-title">Laravel</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="96" data-delay="0" data-type="%">
            <div class="skill-title">Django</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="85" data-delay="0" data-type="%">
            <div class="skill-title">Devops</div>
        </div>
     </div>
     </div>
     </div>

   <div class="row mt-80">
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="97" data-delay="0" data-type="%">
            <div class="skill-title">Android</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="74" data-delay="0" data-type="%">
            <div class="skill-title">Kotlin</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="88" data-delay="0" data-type="%">
            <div class="skill-title">Python</div>
        </div>
     </div>
     </div>
     </div>

   </div>
</section>

<section class="page-section-ptb gray-bg">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h6>FriendBot </h6>
            <h2 class="title-effect">Equipo de Desarrollo</h2>
          </div>
       </div>
    </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 sm-mb-30">
           <div class="team team-list">
              <div class="team-photo">
                <img class="img-fluid mx-auto" src="{{ asset('frontend/images/luis.png')}}" alt="Luis Angel Quispe Limachi">
              </div>
              <div class="team-description">
                <div class="team-info">
                     <h5><a href="team-single.html">Luis A. Quispe Limachi</a></h5>
                     <span>Mobile, frontend, Backend</span>
                </div>
                <div class="team-contact">
                  <span class="call"> +(591) 71978309 </span>
                  <span class="email"> <i class="fa fa-envelope-o"></i> @luisangel</span>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="https://www.facebook.com/luisangel.quispe1" target="_blank"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="https://www.facebook.com/luisangel.quispe1" target="_blank"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="https://www.facebook.com/luisangel.quispe1" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      <li class="social-linkedin"><a href="https://www.facebook.com/luisangel.quispe1" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
          <div class="col-lg-6 col-md-6">
           <div class="team team-list">
              <div class="team-photo">
                <img class="img-fluid mx-auto" src="{{ asset('frontend/images/alfredo.png')}}" alt="Alfredo Callizaya">
              </div>
              <div class="team-description">
                <div class="team-info">
                     <h5><a href="team-single.html">Alfredo Callizaya Gutierrez</a></h5>
                     <span>Backend Developer</span>
                </div>
                <div class="team-contact">
                  <span class="call"> +(591) 73053045</span>
                  <span class="email"> <i class="fa fa-envelope-o"></i> @alfredynho</span>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="https://www.facebook.com/Alfr3dynho" target="_blank"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="https://www.facebook.com/Alfr3dynho" target="_blank"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="https://www.facebook.com/Alfr3dynho" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      <li class="social-linkedin"><a href="https://www.facebook.com/Alfr3dynho" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
       </div>
  </div>
</section>

<section id="raindrops" class="raindrops" style="height: 50px;"></section>

<section class="contact-box contact-box-top theme-bg">
  <div class="container">
    <div class="row pt-20 pb-40">
      <div class="col-md-4 sm-mb-30">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-android text-white"></i>
          </div>
          <div class="contact-info">
            <h5 class="text-white">Luis Angel Quispe Limachi</h5>
            <span class="text-white">luisangelql10@gmail.com</span>
          </div>
        </div>
      </div>
      <div class="col-md-4 sm-mb-30">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-linux text-white"></i>
          </div>
          <div class="contact-info">
            <h5 class="text-white">Alfredo Callizaya Gutierrez</h5>
            <span class="text-white">alfredynho.cg@gmail.com</span>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-email text-white"></i>
          </div>
          <div class="contact-info">
            <h5 class="text-white">friendbotall@gmail.com</h5>
          <span class="text-white">friendBot</span>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>


@endsection

@section('extrajs')


  @if (Session::has('inscription'))
      <!-- Pushboar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>{{ ucfirst(Session::get('inscription')) }} acaba de realizar su inscripcion con Exito ⚡ ,  Enviaremos un correo de confirmación de su inscripción al Evento</h5>
                          <button  data-pushbar-close class="button button-border black x-small">Amper SRL - Eficiencia Energetica </button>
                          </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- end pushbar -->
  @endif


    @if(count($errors)>0)
      <!-- Pushboar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>
                            @foreach ($errors->all() as $error)
                                {{ $error}}
                            @endforeach
                          </h5>
                          <button  data-pushbar-close class="button button-border black x-small">Amper SRL - Eficiencia Energetica </button>
                          </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- Pushboar -->

    @endif

  @if (Session::has('suscription'))

      <!-- Pushboar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>{{ (Session::get('suscription')) }} suscrito Correctamente ⚡ , ahora estaras informado de todos nuestros eventos </h5>
                          <button  data-pushbar-close class="button button-border black x-small">Amper SRL - Eficiencia Energetica </button>
                          </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- end pushbar -->
  @endif

    <script src="{{ asset('frontend/js/axios.min.js') }}"></script>

    <script src="{{ asset('frontend/js/leaflet.js') }}"></script>

    <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=BOllcdYjn6GgcvzO7Ut7Z14BsWj5Qz5Q"></script>

    <script>

        var mapLayer = MQ.mapLayer();

        var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
            thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; ' + osmLink + ' Contributors',
            landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
            thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

        var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib}),
            landMap = L.tileLayer(landUrl, {attribution: thunAttrib});

		var baseLayers = {
			"OSM Mapnik": osmMap,
			"Landscape": landMap
		};


    var map = L.map('mapid', {
        layers: mapLayer,
    }).setView([
        {{ config('leaflet.map_center_latitude') }},
        {{ config('leaflet.map_center_longitude') }}],
        {{ config('leaflet.zoom_level') }}
    );

    map.dragging.disable();
    map.scrollWheelZoom.disable();


    var greenIcon = L.icon({
        iconUrl: "{{ asset('frontend/images/maps_amper.webp') }}",

        iconSize:     [55, 70],
        shadowSize:   [50, 64],
        iconAnchor:   [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor:  [-3, -76]
    });


    axios.get('{{ route('api.maps.index') }}')
    .then(function (response) {
        console.log(response.data);
        L.geoJSON(response.data, {
            pointToLayer: function(geoJsonPoint, latlng) {
                return L.marker(latlng, {icon: greenIcon});
            }
        })
        .bindPopup(function (layer) {
            return layer.feature.properties.map_popup_content;
        }).addTo(map);
    })
    .catch(function (error) {
        console.log(error);
    });

    var baseUrl = "{{ url('/') }}";

    L.control.layers({
        'Mapa': mapLayer,
        'Hibrido': MQ.hybridLayer(),
        'Satelital': MQ.satelliteLayer(),
        'Negro': MQ.darkLayer(),
        'Blanco': MQ.lightLayer()
    }).addTo(map);

    </script>



@endsection

@extends('frontend.layout')

@section('title', __('Amper Srl.'))

@section('extracss')

@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' data-jarallax-video="https://www.youtube.com/watch?v=BTWRXjyMrYg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="col-lg-8">
            <h3 class="text-white">Somos el GO - TO del contról y manejo de energía a nivel NACIONAL </h3>
            <p class="text-white text-justify">
            AMPER fue fundada en julio de 1992 en la ciudad de La Paz, con el objetivo
            de brindar soluciones de energía en baja y media tensión mediante productos
            y servicios de la mejor calidad. A través del tiempo nos expandimos abriendo
            una oficina regional en Santa Cruz de la Sierra, Estados Unidos y ahora en Ecuador.
            </p>
        </div>
     </div>
     </div>
  </div>
</section>

<section class="page-section-ptb">
  <div class="container">
       <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="section-title text-center">
                <h2>Amper SRL</h2>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="feature-text left-icon mt-60 xs-mt-30">
            <div class="feature-icon">
                <span class="ti-bolt theme-color" aria-hidden="true"></span>
            </div>
            <div class="feature-info">
                <h5>Amper</h5>
                <p class="text-center">Somos representantes exclusivos para Bolivia de importantes compañas a nivel mundial: DEHN (Alemania), CSB (Taiwan), EATON POWERWARE (USA), STULZ(Alemania), NOJA POWER (Australia)</p>
            </div>
            </div>
         </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="feature-text left-icon mt-60 xs-mt-30">
              <div class="feature-icon">
                    <span class="ti-bookmark theme-color" aria-hidden="true"></span>
              </div>
               <div class="feature-info">
                    <h5>Visión</h5>
                    <p class="text-center">Liderar el Mercado nacional con un desarrollo sostenible de confiablidad, innovación y satisfacción al cliente. </p>
                </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="feature-text left-icon mt-60 xs-mt-30">
              <div class="feature-icon">
                <span class="ti-flag-alt theme-color" aria-hidden="true"></span>
              </div>
               <div class="feature-info">
                    <h5 class="text-back">Misión</h5>
                    <p class="text-center">Ofrecer productos de calidad otorgando un eficiente servicio post venta, evitando demoras o problemas que incidan en la productividad de nuestros clientes. </p>
                </div>
          </div>
        </div>
     </div>
  </div>
</section>

<section class="key-features white-bg page-section-ptb">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="section-title text-center">
          <h6>Amper SRL.</h6>
          <h2>Actualmente la oferta se encuentra en las siguientes areas:</h2>
        </div>
       </div>
      </div>
    <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="feature-text round shadow text-center mb-40">
            <div class="feature-icon">
                <img src="{{ asset('frontend/images/servicio.webp' )}}"  height="150" width="150" alt="Amper Servicio Técnico ">
            </div>
            <div class="feature-info">
            <h4 class="text-back pb-10">Servicio Técnico</h4>
            <p>El Área de servicio técnico especializado, cuenta con varios planes que pueda adecuarse a las necesidades de todos nuestros clientes.</p>
             <a class="button button-border x-small" href="/soluciones">ver mas ...</a>
          </div>
         </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="feature-text round shadow text-center mb-40">
            <div class="feature-icon">
                <img src="{{ asset('frontend/images/tension.webp' )}}"  height="150" width="150" alt="Amper Servicio TÃ©cnico ">
            </div>
            <div class="feature-info">
            <h4 class="text-back pb-10">Media Tensión</h4>
            <p>Amper SRL. tiene productos en media tensión complementarios a los reconectadores ...</p><br>
             <a class="button button-border x-small" href="http://www.amperonline.com/unidad-de-media-tensi%C3%B3n" target="_blank">ver mas ...</a>
          </div>
         </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="feature-text round shadow text-center mb-40">
            <div class="feature-icon">
                <img src="{{ asset('frontend/images/distribucion.webp' )}}"  height="150" width="150" alt="Amper Servicio TÃ©cnico ">
            </div>
            <div class="feature-info">
            <h4 class="text-back pb-10">Distribución de Energí­a</h4>
            <p>La unidad de distribución de energí­a se encarga del dimensionamiento estructurado y normado de instalaciones eléctricas, medianamente la utilización de Softwares de ultima tecnología ...</p>
             <a class="button button-border x-small" href="http://www.amperonline.com/unidad-de-distribucion-de-energia" target="_blank">ver mas ...</a>
          </div>
         </div>
        </div>
     </div>
     <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="feature-text round shadow text-center xs-mb-40">
            <div class="feature-icon">
                <img src="{{ asset('frontend/images/infraestructura.webp' )}}"  height="150" width="150" alt="Amper Servicio TÃ©cnico ">
            </div>
            <div class="feature-info">
            <h4 class="text-back pb-10">Infraestructura para Centros de Datos</h4>
            <p>Racks para bancos de baterías diseñados a medida para todo tipo de requerimiento en salas de energí­a, tanto para sistemas UPS como en plantas de energía DC.</p>
             <a class="button button-border x-small" href="http://www.amperonline.com/unidad-de-infraestructura-para-centros-de-datos" target="_blank">ver mas ...</a>
          </div>
         </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="feature-text round shadow text-center xs-mb-40">
            <div class="feature-icon">
                <img src="{{ asset('frontend/images/calidad_energia.webp' )}}"  height="150" width="150" alt="Amper Servicio TÃ©cnico ">
            </div>
            <div class="feature-info">
            <h4 class="text-back pb-10">Calidad de Energí­a</h4>
            <p>Proporcionamos soluciones completas a partir de la identificación de requerimientos, diseño personalizado, provisión e instalación de equipos, todo con el debido respaldo de post venta.</p>
             <a class="button button-border x-small" href="http://www.amperonline.com/unidad-de-calidad-de-energia" target="_blank">ver mas ...</a>
          </div>
         </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="feature-text round shadow text-center xs-mb-40">
            <div class="feature-icon">
                <img src="{{ asset('frontend/images/soluciones.webp' )}}"  height="150" width="150" alt="Amper Servicio TÃ©cnico ">
            </div>
            <div class="feature-info">
            <h4 class="text-back pb-10">Soluciones</h4>
            <p>Las mejores y más modernas soluciones para Data Centers, a la medida de sus necesidades, garantizando máxima disponibilidad, redundancia operativa, seguridad y continuidad en su renovación tecnológica.</p>
             <a class="button button-border x-small" href="http://www.amperonline.com/soluciones" target="_blank">ver mas ...</a>
          </div>
         </div>
        </div>

     </div>
   </div>
</section>

 <section class="page-section-ptb">
   <div class="container">

    <div class="row">
     <div class="col-md-12">
         <div class="section-title text-center">
            <h2>¿Sabias que tenemos un showroom en el justo centro de la ciudad de La Paz?</h2>
          </div>
       </div>
    </div>

     <div class="row mt-60">
        <div class="col-lg-12 col-md-12">
          <div class="isotope columns-3 popup-gallery">

        @if (count($gallery)>= 1)

            @foreach ($gallery as $ga)

              <div class="grid-item">
                  <div class="portfolio-item">
                    <img class="image" src="{{ Storage::url('Gallery/'.$ga->image) }}" alt="{{ $ga->title }}" style="height:240px;width:500px;">
                     <div class="portfolio-overlay">
                        <span class="text-white">{{ Str::limit($ga->title,95) }} </span>
                      </div>
                        <a class="popup portfolio-img" href="{{ Storage::url('Gallery/'.$ga->image) }}" ><i class="fa fa-arrows-alt"></i></a>
                </div>
               </div>

            @endforeach

        @else
            <div class="mb-200">
                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                <strong>Sin Registros!</strong> Nos encontramos preparando el mejor contenido para ti ...
                </div>
            </div>
        @endif

            </div>
          </div>
     </div>

  <div class="row">
  <div class="col-lg-12 col-lg-12">
      <div class="entry-pagination">
        <nav aria-label="Page navigation example text-center">
            <ul class="pagination justify-content-center">
                {{ $gallery->links() }}
            </ul>
          </nav>
      </div>
    </div>
   </div>
   </div>
 </section>

@endsection


@section('extrajs')


@endsection

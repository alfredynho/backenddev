@extends('frontend.layout')

@section('title', __('Amper Srl Detalle Blog'))

@section('extracss')

@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Detalle Blog</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a>Detalle Blog</a></li>
       </ul>
     </div>
     </div>
  </div>
</section>

<section class="page-sidebar page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-lg-9 col-md-8 page-content">
         <h3 class="theme-color">{{ $post->title }}</h3>
         <p class="text-justify">{!! $post->description !!}</p>
        <div class="col-sm-6 xs-mb-30">
        <h4 class="mb-20">Compartir</h4>
            <div class="social-icons medium color clearfix">
                <ul>
                    <li class="social-facebook"><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-twitter"><a href="https://twitter.com/intent/tweet?text=EficienciaEnergetica&url={{ url()->full() }}&hashtags=#Amper" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-whatsapp"><a href="https://api.whatsapp.com/send?text=Eficiencia%20Energetica%20AmperSRL%20{{ url()->full() }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                </ul>
            </div>
        </div>

        <div id="disqus_thread"></div>

       </div>

       <div class="col-lg-3 col-md-4">
          <div class="sidebar-widget">
            <h6 class="mb-20">Encuentranos en Facebook</h6>
                <div class="widget-search">
                    <div class="fb-page" data-href="https://www.facebook.com/ampersrl/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ampersrl/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ampersrl/">AMPER SRL</a></blockquote></div>
                </div>
          </div>


          <div class="sidebar-widget">
             <h6 class="mt-40 mb-20">Testimonios</h6>
          </div>

            <div class="sidebar-widget">
           <div class="owl-carousel" data-nav-dots="false" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
            @foreach ($testimony as $ts)
               <div class="item">
                 <div class="testimonial-widget">
                   <div class="testimonial-content">
                     <p>{{ Str::limit($ts->message,110) }}</p>
                   </div>
                   <div class="testimonial-info mt-20">
                     <div class="testimonial-avtar">
                       <img class="img-fluid" src="{{ asset('frontend/images/amper_user.webp') }}" alt="">
                     </div>
                     <div class="testimonial-name">
                       <strong>{{ $ts->author }}</strong>
                       <span>{{ $ts->created_at->format('d-m-Y') }}</span>
                     </div>
                   </div>
                 </div>
               </div>
            @endforeach

            </div>
          </div>


        <div class="sidebar-widget">
            <h6 class="mt-40 mb-20">Publicaciones Recientes </h6>

            @foreach ($blog_recent as $bg)

            <div class="recent-post clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="{{ asset('frontend/images/amper_user.webp') }}" alt="">
              </div>
             <div class="recent-post-info">
              <a>{{ $bg->title }}</a>
              <span><i class="fa fa-calendar-o"></i> {{ $bg->created_at->format('d-m-Y') }}</span>
             </div>
            </div>

            @endforeach

        </div>


      </div>
    </div>
  </div>
</section>

@endsection

@section('extrajs')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0&appId=383264375339567&autoLogAppEvents=1"></script>

    <script>
    (function() {
    var d = document, s = d.createElement('script');
    s.src = 'https://amperonline.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

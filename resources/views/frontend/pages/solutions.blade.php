@extends('frontend.layout')

@section('title', __('Amper Srl Soluciones'))

@section('extracss')

@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Soluciones Amper</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Soluciones</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<section class="page-section-ptb">
  <div class="container">
     <div class="row">
           <div class="col-lg-6">
         <div class="who-we-are-left">
          <div class="item"><img class="img-fluid full-width" src="{{ asset('frontend/images/blog_amper.webp') }}" alt="">
         </div>
        </div>
      </div>
      <div class="col-lg-6 sm-mt-30">
        <div class="section-title">
            <h2 class="title-effect">Unidad de infraestructura para centros de datos</h2>
          </div>
          <p  class="text-justify">El diseño de un data center involucra varias especialidades, como: Ingeniería de
            redes, electricidad, cableado estructurado, seguridad de acceso y ambiental, entre
            los principales.
            El datacenter es el lugar donde se instalan todos equipos de comunicaciones,
            servidores, sistemas de almacenamiento de una empresa.
            No es simplemente una sala de equipo electrónicos, es un lugar que hay que
            diseñarlo siguiendo los estándares internacionales.
            </p>
      </div>
     </div>
  </div>
</section>


<section class="page-section-pb">
  <div class="container">
    <div class="row">
        <div class="col-lg-3 col-sm-6 sm-mb-30">
           <div class="team team-hover">
              <div class="team-photo">
                <img class="img-fluid mx-auto" src="{{ asset('frontend/images/blog_amper.webp') }}" alt="">
              </div>
              <div class="team-description">
                <div class="team-info">
                     <h5><a href="team-single.html">UPS TRIFÁSICOS</a></h5>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
          <div class="col-lg-3 col-sm-6 sm-mb-30">
           <div class="team team-hover">
              <div class="team-photo">
                <img class="img-fluid mx-auto" src="{{ asset('frontend/images/blog_amper.webp') }}" alt="">
              </div>
              <div class="team-description">
                <div class="team-info">
                     <h5><a href="team-single.html">SISTEMAS DC EATON</a></h5>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
          <div class="col-lg-3 col-sm-6 xs-mb-30">
           <div class="team team-hover">
              <div class="team-photo">
                <img class="img-fluid mx-auto" src="{{ asset('frontend/images/blog_amper.webp') }}" alt="">
              </div>
              <div class="team-description">
                <div class="team-info">
                     <h5><a href="team-single.html">TRANSFORMADORES DE AISLAMIENTO</a></h5>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
          <div class="col-lg-3 col-sm-6">
           <div class="team team-hover">
              <div class="team-photo">
                <img class="img-fluid mx-auto" src="{{ asset('frontend/images/blog_amper.webp') }}" alt="">
              </div>
              <div class="team-description">
                <div class="team-info">
                     <h5><a href="team-single.html">cables aislados</a></h5>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
       </div>
  </div>
</section>

@endsection

@section('extrajs')


@endsection

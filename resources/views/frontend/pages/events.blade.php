@extends('frontend.layout')

@section('title', __('Amper Srl Eventos'))

@section('extracss')
    <link rel="stylesheet" href="{{ asset('frontend/css/pushbar.css') }}">
@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Eventos</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a>Eventos</a></li>
       </ul>
     </div>
     </div>
  </div>
</section>

<section class="our-blog gray-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h6>Eficiencia Energetica</h6>
            <h2>Eventos</h2>
          </div>
       </div>
    </div>
    </div>
 </section>

<section class="blog white-bg page-section-ptb">
  <div class="container">
    <div>
     <div class="col-lg-12 col-md-12">

    @include('frontend.partials.inscripcion')

    @if (count($events)>= 1)

        @foreach ($events as $eve)

            <div class="blog-entry mb-50">
                <div class="blog-detail">

                    <div class="port-post-photo">
                        @if ($eve->image)
                            <img src="{{ Storage::url('Events/'.$eve->image) }}"  style="height:150px;width:150px;" alt="{{ $eve->title }}">
                        @else
                            <img src="{{ asset('frontend/images/default_event.webp') }}" alt="{{ $eve->title }}">
                        @endif
                    </div>

                    <div class="entry-title mb-10">
                        <a href="{{ route('detail_event', [$eve->slug]) }}">{{ $eve->title }}</a>
                    </div>
                    <div class="entry-meta mb-10">
                        <ul>
                            <li><a href="{{ route('detail_event', [$eve->slug]) }}"><i class="fa fa-calendar-o"></i> {{ $eve->created_at->format('d-m-Y') }}</a></li>
                        </ul>
                    </div>
                    <div class="entry-content">
                        <p class="text-justify"> {{ Str::limit($eve->description,200) }} </p>
                    </div>
                    <div class="entry-share clearfix">
                        <div class="entry-button">
                            <a class="button arrow" href="{{ route('detail_event', [$eve->slug]) }}">mas detalles<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="social list-style-none float-right">
                            <button class="button icon white xs-mt-10" data-toggle="modal" data-target=".bd-example-modal-lg"> ¡Registrarme ahora!</button>
                        </div>
                    </div>
                </div>

            </div>

        @endforeach

        @else
            <div class="mb-200">
                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                <strong>Sin Registros!</strong> Nos encontramos preparando el mejor contenido para ti ...
                </div>
            </div>
        @endif

    <div class="row">
    <div class="col-lg-12 col-lg-12">
        <div class="entry-pagination">
            <nav aria-label="Page navigation example text-center">
                <ul class="pagination justify-content-center">
                    {{ $events->links() }}
                </ul>
            </nav>
        </div>
        </div>
    </div>

    </div>
    </div>
    </div>

</section>

<section class="page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-12">
       <h4><strong></strong>REVISA NUESTRO CALENDARIO DE EVENTOS!</strong></h4>
         <div id='calendar'></div>
       </div>
    </div>
  </div>
</section>

@endsection

@section('extrajs')
    <script src="{{ asset('frontend/js/pushbar.js') }}"></script>

    <script type="text/javascript">
        var pushbar = new Pushbar({
            blur:true,
            overlay:true
        });

        var interval = setTimeout(function(){
            pushbar.open('pushbar-notification');
        },100);
    </script>
@endsection

@extends('frontend.layout')

@section('title', __('Amper Srl Contactos'))

@section('extracss')
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/leaflet.css') }}" />

    <style>
        #mapid { height: 380px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#1A1A1A;
            color: #ffffff;
        }
    </style>

    <style>
    .pushbar {
        background:#FFFFFF;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#333333;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    </style>

    <link rel="stylesheet" href="{{ asset('frontend/css/pushbar.css') }}">

@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Conoce nuestras instalaciones</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Contactos</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>

<div id="mapid"></div>

<section id="contact-us" class="white-bg page-section-ptb bg-overlay-black-70">
  <div class="container">
    <div class="row">
      <div class="col-md-6 contact-3 sm-mt-40">
        <div class="section-title dark-bg">
            <h2 class="title">¡Contactanos!</h2>
        </div>

        <div>
        @foreach ($errors->all() as $error)
            <div class="mb-200">
                <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                    <strong>{{ $error }}</strong>
                </div>
            </div>
        @endforeach
        </div>

        {!! Form::open(['route' => 'dashboard.contactos.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}
          <div class="contact-form transparent-form clearfix">

            <div class="section-field">
              <input id="nombre_c" type="text" placeholder="Nombre Completo*" class="form-control"  name="nombre_c" required value="{{old('nombre_c')}}">
            </div>

            <div class="section-field">
              <input type="email_c" placeholder="Email*" class="form-control" name="email_c" id="email_c" required value="{{old('email_c')}}">
            </div>

            <div class="section-field">
              <input type="number" placeholder="Teléfono*" class="form-control" name="celular_c" id="celular_c" required value="{{old('celular_c')}}">
            </div>

            <div class="section-field textarea">
              <textarea class="input-message form-control" placeholder="Comentario*"  rows="7" name="comentario_c" required></textarea>
            </div>

            <input type="hidden" name="action" value="sendEmail"/>
            <button id="submit" name="submit" type="submit" value="Send" class="button button-border white"><span>Enviar Mensaje</span></button>

          </div>
        {{Form::Close()}}
      </div>

      <div class="col-md-6">
        <div class="contact-3-info text-white">
          <div class="clearfix">
            <div class="section-title dark-bg text-left"><br><br>
              <h2 class="title">OFICINAS EN BOLIVIA:</h2>
            </div>
            <p class="mb-50 text-white">
                LA PAZ: <br>
                Calle Gosálves, La Paz , Bolivia
            </p>

            <p class="mb-50 text-white">
                SHOWROOM: <br>
                Calle México Nro. 1790.<br>
                Edif. maria reyna P.B <br>
                Telf: 2486584 - 2486597
            </p>

            <p class="mb-50 text-white">
                SANTA CRUZ: <br>
                Calle Los Claveles Nro: 505, <br>
                Sirari<br>
                Telf: 3419495
            </p>

            <div class="social-icons social-border rounded color-hover mt-50">
              <ul>
                <li class="social-facebook"><a href="{{ getSocial("FACEBOOK") }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li class="social-youtube"><a href="{{ getSocial("YOUTUBE") }}" target="_blank"><i class="fa fa-youtube"></i></a></li>
                <li class="social-linkedin"><a href="{{ getSocial("WHASAPP_LA_PAZ") }}" target="_blank"><i class="fa fa-whatsapp"></i> </a></li>
                <li class="social-instagram"><a href="{{ getSocial("INSTAGRAM") }}" target="_blank"><i class="fa fa-instagram"></i> </a></li>
                <li class="social-dropbox"><a href="{{ getSocial("LINKEDIN") }}" target="_blank"><i class="fa fa-linkedin"></i> </a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

@endsection

@section('extrajs')

    @if (Session::has('contactos'))
        <!-- Pushboar -->
          <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
            <div class="btn-close">
               	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
            </div>
                <div class="contenedor">
                    <div class="main-title">
                        <div class="title-main-page">
                        <div class="container text-center">
                            <h5>{{ (Session::get('contactos')) }} Se envio su correo correctamente ⚡ </h5>
                            <button  data-pushbar-close class="button button-border black x-small">Amper SRL - Eficiencia Energetica </button>
                            </div>
                        </div>
                    </div>
                    <br>
                    </div>
                </div>
          </div>
        <!-- end pushbar -->
    @endif


    <script src="{{ asset('frontend/js/axios.min.js') }}"></script>

    <script src="{{ asset('frontend/js/leaflet.js') }}"></script>

    <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=BOllcdYjn6GgcvzO7Ut7Z14BsWj5Qz5Q"></script>

    <script>

        var mapLayer = MQ.mapLayer();

        var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
            thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; ' + osmLink + ' Contributors',
            landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
            thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

        var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib}),
            landMap = L.tileLayer(landUrl, {attribution: thunAttrib});

		var baseLayers = {
			"OSM Mapnik": osmMap,
			"Landscape": landMap
		};


    var map = L.map('mapid', {
        layers: mapLayer,
    }).setView([
        {{ config('leaflet.map_center_latitude') }},
        {{ config('leaflet.map_center_longitude') }}],
        {{ config('leaflet.zoom_level') }}
    );

    map.dragging.disable();
    map.scrollWheelZoom.disable();


    var greenIcon = L.icon({
        iconUrl: "{{ asset('frontend/images/maps_amper.webp') }}",

        iconSize:     [55, 70],
        shadowSize:   [50, 64],
        iconAnchor:   [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor:  [-3, -76]
    });


    axios.get('{{ route('api.maps.index') }}')
    .then(function (response) {
        console.log(response.data);
        L.geoJSON(response.data, {
            pointToLayer: function(geoJsonPoint, latlng) {
                return L.marker(latlng, {icon: greenIcon});
            }
        })
        .bindPopup(function (layer) {
            return layer.feature.properties.map_popup_content;
        }).addTo(map);
    })
    .catch(function (error) {
        console.log(error);
    });

    var baseUrl = "{{ url('/') }}";

    L.control.layers({
        'Mapa': mapLayer,
        'Hibrido': MQ.hybridLayer(),
        'Satelital': MQ.satelliteLayer(),
        'Negro': MQ.darkLayer(),
        'Blanco': MQ.lightLayer()
    }).addTo(map);

    </script>


@endsection('extrajs')

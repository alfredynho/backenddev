@extends('frontend.layout')

@section('title', __('Amper Blog'))

@section('extracss')

@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Blog</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a >Blog</a></li>
       </ul>
     </div>
     </div>
  </div>
</section>

<section class="our-blog gray-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h6>Eficiencia Energetica</h6>
            <h2>Nuevos Artículos informativos y casos de éxito</h2>
          </div>
       </div>
    </div>
    </div>
 </section>

<section class="blog blog-grid-3-column white-bg page-section-ptb">
  <div class="container">
    <div class="row">

    @if (count($blog)>= 1)
        @foreach ($blog as $bg)
            <div class="col-lg-4 col-md-4">
                <div class="blog-entry mb-50">
                    <div class="blog-box blog-2 h-100 white-bg">
                        <img class="img-fluid" src="{{ Storage::url('Blog/'.$bg->image) }}" style="height:240px;width:500px;" alt="{{ $bg->title }}">
                        <div class="blog-info">
                        <span class="post-category"><a href="{{ route('detail_blog', [$bg->slug]) }}">{{ strtoupper($bg->category_title) }}</a></span>
                        <h4> <a href="{{ route('detail_blog', [$bg->slug]) }}"> {{ Str::limit($bg->title,40) }}</a></h4>
                        <p class="text-justify">{{ Str::limit($bg->description,75) }}</p>
                        <span><i class="fa fa-calendar-check-o"></i> {{ $bg->created_at->format('d-m-Y') }} </span>
                        <a class="button icon-color" href="{{ route('detail_blog', [$bg->slug]) }}">ver mas<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    @else
        <div class="mb-200">
            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
            <strong>Sin Registros!</strong> Nos encontramos preparando el mejor contenido para ti ... .
            </div>
        </div>
    @endif

 </div>

  </div>

  <div class="row">
  <div class="col-lg-12 col-lg-12">
      <div class="entry-pagination">
        <nav aria-label="Page navigation example text-center">
            <ul class="pagination justify-content-center">
                {{ $blog->links() }}
            </ul>
          </nav>
      </div>
    </div>
   </div>

  </div>
 </section>

@endsection


@section('extrajs')


@endsection

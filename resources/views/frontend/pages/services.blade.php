@extends('frontend.layout')

@section('title', __('Amper Srl Servicios'))

@section('extracss')
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/leaflet.css') }}" />

    <style>
        #mapid { height: 380px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#1A1A1A;
            color: #ffffff;
        }

        @font-face {
  font-family: 'icomoon';
  src:  url("{{ asset('frontend/fonts/icomoon.eot?i226ha')}}");
  src:  url('fonts/icomoon.eot?i226ha#iefix') format('embedded-opentype'),
    url('fonts/icomoon.ttf?i226ha') format('truetype'),
    url('fonts/icomoon.woff?i226ha') format('woff'),
    url('fonts/icomoon.svg?i226ha#icomoon') format('svg');
  font-weight: normal;
  font-style: normal;
}

[class^="icon-"], [class*=" icon-"] {
  /* use !important to prevent issues with browser extensions that change fonts */
  font-family: 'icomoon' !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;

  /* Better Font Rendering =========== */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.icon-facebook:before {
    font-family: "Font Awesome 5 Free";
    font-weight: 900;
    content: "\f007";

}
.icon-instagram:before {
  content: "\ea92";
}
.icon-twitter:before {
  content: "\ea96";
}
.icon-youtube:before {
  content: "\ea9d";
}



.social-bar {
	position: fixed;
	right: 0;
	top: 35%;
	font-size: 1.5rem;
	display: flex;
	flex-direction: column;
	align-items: flex-end;
	z-index: 100;
}

.icon {
	color: white;
	text-decoration: none;
	padding: .7rem;
	display: flex;
	transition: all .5s;
}

.icon-facebook {
	background: #2E406E;
}

.icon-twitter {
	background: #339DC5;
}

.icon-youtube {
	background: #E83028;
}

.icon-instagram {
	background: #3F60A5;
}

.icon:first-child {
	border-radius: 1rem 0 0 0;
}

.icon:last-child {
	border-radius: 0 0 0 1rem;
}

.icon:hover {
	padding-right: 3rem;
	border-radius: 1rem 0 0 1rem;
	box-shadow: 0 0 .5rem rgba(0, 0, 0, 0.42);
}


    </style>

@endsection('extracss')

@section('content')


<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Conoce nuestras instalaciones</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Servicios</span> </li>
       </ul>
     </div>
     </div>
  </div>
</section>


  <div class="social-bar">
    <a href="https://www.facebook.com/DevCode.la" class="icon icon-facebook" target="_blank"></a>
    <a href="https://twitter.com/DevCodela" class="icon icon-twitter" target="_blank"></a>
    <a href="https://www.youtube.com/c/devcodela" class="icon icon-youtube" target="_blank"></a>
    <a href="https://www.instagram.com/devcodela/" class="icon icon-instagram" target="_blank"></a>
  </div>

<section class="blog white-bg page-section-ptb">
  <div class="container">
    <div class="row">
        <div class="col-lg-3">
          <div class="sidebar-widget">
            <h6 class="mb-20">Search</h6>
              <div class="widget-search">
                <i class="fa fa-search"></i>
                <input type="search" class="form-control" placeholder="Search...." />
              </div>
          </div>
          <div class="sidebar-widget">
             <h6 class="mt-40 mb-20">About the blog</h6>
             <p>We are the <strong> webster </strong> dolor sit ametLorem Ipsum Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, <br/> <br/>
              Consequat ipsum, nec sagittis sem nibh id elit nibh vel velit auctor aliquet. sem nibh  Aenean sollicitudin, </p>
          </div>
        <div class="sidebar-widget">
            <h6 class="mt-40 mb-20">Recent Posts </h6>
            <div class="recent-post clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/01.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Nibh vel velit auctor aliquet. sem nibh Aenean</a>
              <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
             </div>
            </div>
            <div class="recent-post clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/02.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Nibh vel velit auctor aliquet. sem nibh Aenean</a>
              <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
             </div>
            </div>
            <div class="recent-post clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/03.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Nibh vel velit auctor aliquet. sem nibh Aenean</a>
              <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
             </div>
            </div>
        </div>
        <div class="sidebar-widget clearfix">
          <h6 class="mt-40 mb-20">Archives</h6>
          <ul class="widget-categories">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> December 2018</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> November 2018</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> October 2018</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> September 2018</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> August 2018</a></li>
          </ul>
      </div>
      <div class="sidebar-widget">
       <h6 class="mt-40 mb-20">Tags</h6>
        <div class="widget-tags">
         <ul>
          <li><a href="#">Bootstrap</a></li>
          <li><a href="#">HTML5</a></li>
          <li><a href="#">Wordpress</a></li>
          <li><a href="#">CSS3</a></li>
          <li><a href="#">Creative</a></li>
          <li><a href="#">Multipurpose</a></li>
          <li><a href="#">Bootstrap</a></li>
          <li><a href="#">HTML5</a></li>
          <li><a href="#">Wordpress</a></li>
        </ul>
      </div>
     </div>
      <div class="sidebar-widget">
          <h6 class="mt-40 mb-20">Meta</h6>
          <ul class="widget-categories">
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Log in</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Entries RSS</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Comments RSS </a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> September 2018</a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> WordPress.org</a></li>
          </ul>
      </div>
      <div class="sidebar-widget">
        <h6 class="mt-40 mb-20">Testimonials</h6>
         <div class="owl-carousel" data-nav-dots="false" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
             <div class="item">
               <div class="testimonial-widget">
                 <div class="testimonial-content">
                   <p>Webster provide me consectetur adipisicing elit. Voluptatum dignissimos amet numquam at est eum libero repellat reiciendis! Accusamus quibusdam.</p>
                 </div>
                 <div class="testimonial-info mt-20">
                   <div class="testimonial-avtar">
                     <img class="img-fluid" src="images/team/01.jpg" alt="">
                   </div>
                   <div class="testimonial-name">
                     <strong>Michael Bean</strong>
                     <span>Project Manager</span>
                   </div>
                 </div>
               </div>
             </div>
             <div class="item">
               <div class="testimonial-widget">
                 <div class="testimonial-content">
                   <p>I am happy with webster service adipisicing elit. Voluptatum dignissimos amet libero repellat reiciendis! Accusamus quibusdam numquam at est eum. </p>
                 </div>
                 <div class="testimonial-info mt-20">
                   <div class="testimonial-avtar">
                     <img class="img-fluid" src="images/team/01.jpg" alt="">
                   </div>
                   <div class="testimonial-name">
                     <strong>Paul Flavius</strong>
                     <span>Design</span>
                   </div>
                 </div>
               </div>
             </div>
          </div>
      </div>
      <div class="sidebar-widget-widget">
        <h6 class="mt-40 mb-20">Quick contact</h6>
          <form class="gray-form">
              <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Name">
              </div>
              <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputphone" placeholder="Email">
              </div>

              <div class="form-group">
                  <textarea class="form-control" rows="4" placeholder="message"></textarea>
              </div>
             <a class="button" href="#">Submit</a>
          </form>
      </div>
       <div class="sidebar-widget">
        <h6 class="mt-40 mb-20">Photo gallery</h6>
          <div class="widget-gallery popup-gallery clearfix">
            <ul>
              <li>
                <a class="portfolio-img" href="images/portfolio/small/01.jpg">
                    <img class="img-fluid" src="images/portfolio/small/01.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/02.jpg">
                    <img class="img-fluid" src="images/portfolio/small/02.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/03.jpg">
                    <img class="img-fluid" src="images/portfolio/small/03.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/04.gif">
                    <img class="img-fluid" src="images/portfolio/small/04.gif" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/05.jpg">
                    <img class="img-fluid" src="images/portfolio/small/05.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/06.jpg">
                    <img class="img-fluid" src="images/portfolio/small/06.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/07.jpg">
                    <img class="img-fluid" src="images/portfolio/small/07.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/08.gif">
                    <img class="img-fluid" src="images/portfolio/small/08.gif" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/09.jpg">
                    <img class="img-fluid" src="images/portfolio/small/09.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/10.jpg">
                    <img class="img-fluid" src="images/portfolio/small/10.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/01.jpg">
                    <img class="img-fluid" src="images/portfolio/small/01.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/02.jpg">
                    <img class="img-fluid" src="images/portfolio/small/02.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/03.jpg">
                    <img class="img-fluid" src="images/portfolio/small/03.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/04.gif">
                    <img class="img-fluid" src="images/portfolio/small/04.gif" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/05.jpg">
                    <img class="img-fluid" src="images/portfolio/small/05.jpg" alt="">
                  </a>
             </li>
             <li>
                <a class="portfolio-img" href="images/portfolio/small/06.jpg">
                    <img class="img-fluid" src="images/portfolio/small/06.jpg" alt="">
                  </a>
             </li>
            </ul>
          </div>
       </div>
        <div class="sidebar-widget">
         <h6 class="mt-40 mb-20">Newsletter</h6>
          <div class="widget-newsletter">
          <div class="newsletter-icon">
            <i class="fa fa-envelope-o"></i>
          </div>
          <div class="newsletter-content">
            <i>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter. </i>
          </div>
          <div class="newsletter-form mt-20">
              <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Name">
              </div>
             <a class="button btn-block" href="#">Submit</a>
          </div>
        </div>
       </div>
       <div class="sidebar-widget mb-40">
         <h6 class="mt-40 mb-20">Our clients</h6>
          <div class="widget-clients">
            <div class="owl-carousel" data-nav-dots="false" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
              <div class="item">
                <img class="img-fluid mx-auto" src="images/clients/01.png" alt="">
              </div>
              <div class="item">
                <img class="img-fluid mx-auto" src="images/clients/02.png" alt="">
              </div>
              <div class="item">
                <img class="img-fluid mx-auto" src="images/clients/03.png" alt="">
              </div>
              <div class="item">
                <img class="img-fluid mx-auto" src="images/clients/04.png" alt="">
              </div>
            </div>
        </div>
       </div>
   </div>
<!-- ========================== -->
   <div class="col-lg-9">
        <div class="blog-entry mb-50">
          <div class="entry-image clearfix">
              <img class="img-fluid" src="images/blog/01.jpg" alt="">
          </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost With Image</a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
<!-- ================================================ -->
        <div class="blog-entry mb-50">
          <div class="entry-image clearfix">
            <div class="owl-carousel bottom-center-dots" data-nav-dots="ture" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
              <div class="item">
                <img class="img-fluid" src="images/blog/02.jpg" alt="">
              </div>
              <div class="item">
                <img class="img-fluid" src="images/blog/03.jpg" alt="">
              </div>
              <div class="item">
                <img class="img-fluid" src="images/blog/04.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost With slider</a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>

       <!-- ================================================ -->
      <div class="blog-entry mb-50">
        <div class="entry-soundcloud">
                <iframe style="height: 300px; width: 100%;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/118951274&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
              </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost Audio Soundcloud</a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Tempor incididunt ut labore et dolore magna aliqua. Consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>
<!-- ================================================ -->
       <div class="blog-entry mb-50">
          <div class="blog-entry-html-video clearfix">
            <video style="width:100%;height:100%;" id="player1" poster="video/video.jpg" controls preload="none">
              <source type="video/mp4" src="video/video.mp4" />
              <source type="video/webm" src="video/video.webm" />
              <source type="video/ogg" src="video/video.ogv" />
            </video>
          </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost with Self Hosted Video (HTML5 Video)</a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! </p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>

<!-- ================================================ -->
       <div class="blog-entry mb-50">
           <div class="blog-entry-audio audio-video">
            <audio id="player2" src="video/audio.mp3"> </audio>
          </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost with Audio</a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi.</p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>

<!-- ================================================ -->
       <div class="blog-entry mb-50">
           <div class="clearfix">
            <ul class="grid-post">
                 <li>
                  <div class="portfolio-item">
                    <img class="img-fluid" src="images/blog/05.jpg" alt="">
                     </div>
                  </li>
                 <li>
                  <div class="portfolio-item">
                    <img class="img-fluid" src="images/blog/06.jpg" alt="">
                     </div>
                  </li>
                  <li>
                  <div class="portfolio-item">
                     <img class="img-fluid" src="images/blog/07.jpg" alt="">
                     </div>
                  </li>
                  <li>
                  <div class="portfolio-item">
                    <img class="img-fluid" src="images/blog/08.jpg" alt="">
                     </div>
                  </li>
              </ul>
            </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost With Image Grid Gallery Format </a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Incididunt ut labore et dolore magna aliqua Consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>

<!-- ================================================ -->
       <div class="blog-entry mb-50">
            <div class="blog-entry-you-tube">
               <div class="js-video [youtube, widescreen]">
                <iframe src="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" allowfullscreen></iframe>
              </div>
          </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost with Youtube Video </a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>

<!-- ================================================ -->
     <div class="blog-entry mb-50">
            <div class="blog-entry-vimeo">
             <div class="js-video [vimeo, widescreen]">
              <iframe src="https://player.vimeo.com/video/176916362"></iframe>
             </div>
          </div>
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost with vimeo Video </a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>

<!-- ================================================ -->


<div class="blog-entry blockquote mb-50">
          <div class="entry-blockquote clearfix">
              <blockquote class="mt-60 blockquote">
                  The trouble with programmers is that you can never tell what a programmer is doing until it too late. The future belongs to a different kind of person with a different kind of mind: artists, inventors, storytellers-creative and holistic ‘right-brain’ thinkers whose abilities mark the fault line between who gets ahead and who doesn’t.
                  <cite class="text-white"> – Daniel Pink</cite>
              </blockquote>
          </div>
          <div class="blog-detail mt-30">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost With blockquote </a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-share mt-20 clearfix">
                  <div class="entry-button">
                      <a class="button arrow white" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong class="text-white">Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
        </div>

 <!-- ================================================ -->

       <div class="blog-entry mb-50">
          <div class="blog-detail">
              <div class="entry-title mb-10">
                  <a href="#">Blogpost Without Image</a>
              </div>
              <div class="entry-meta mb-10">
                  <ul>
                      <li> <i class="fa fa-folder-open-o"></i> <a href="#"> Design,</a> <a href="#"> HTML5 </a> </li>
                      <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li>
                      <li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2018</a></li>
                  </ul>
              </div>
              <div class="entry-content">
                  <p>A ea maiores corporis. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur, assumenda provident lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! </p>
              </div>
              <div class="entry-share clearfix">
                  <div class="entry-button">
                      <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="social list-style-none float-right">
                      <strong>Share : </strong>
                      <ul>
                          <li>
                              <a href="#"> <i class="fa fa-facebook"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-twitter"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-pinterest-p"></i> </a>
                          </li>
                          <li>
                              <a href="#"> <i class="fa fa-dribbble"></i> </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
       </div>


<!-- ================================================ -->
          <div class="entry-pagination text-center">
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                  <span class="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
          </div>
<!-- ================================================ -->
       </div>
     </div>
    </div>
  </section>

@endsection


@section('extrajs')


@endsection

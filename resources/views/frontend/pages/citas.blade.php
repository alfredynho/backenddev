@extends('frontend.layout')

@section('title', __('Amper Srl Citas'))

@section('extracss')
    <style>
        #mapid { height: 300px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#1A1A1A;
            color: #ffffff;
        }
    </style>

    <style>
    .pushbar {
        background:#FFFFFF;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#333333;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    </style>

    <link rel="stylesheet" href="{{ asset('frontend/css/pushbar.css') }}">
@endsection('extracss')

@section('content')

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('frontend/images/slider2.webp') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Programar Cita</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-home"></i> Inicio</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a>Citas</a></li>
       </ul>
     </div>
     </div>
  </div>
</section>

<section class="our-blog gray-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h6>Eficiencia Energetica</h6>
            <h2>Ponte en contactos con nosotros! Estamos para apoyarte!</h2>
          </div>
       </div>
    </div>
    </div>
 </section>

 <section class="page-section-ptb">
   <div class="container">
     <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="row">
              <div class="col-sm-12">
               <h4 class="mb-40">Formulario de citas</h4>
              <div>
                @foreach ($errors->all() as $error)
                    <div class="mb-200">
                        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                            <strong>{{ $error }}</strong>
                        </div>
                    </div>
                @endforeach
              </div>
                {!! Form::open(['route' => 'dashboard.citas.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <div class="contact-form clearfix">
                   <div class="section-field">
                    <label>Nombre *</label>
                     <input id="name" name="nombre" type="text" placeholder="Nombre*" class="form-control" required value="{{old('nombre')}}">
                 </div>
                   <div class="section-field">
                   <label>Apellidos *</label>
                     <input type="text" name="apellidos" placeholder="Apellidos*" class="form-control" required value="{{old('apellidos')}}">
                  </div>
                    <div class="section-field">
                    <label>Correo Electronico *</label>
                      <input type="email" name="email" placeholder="Correo Electronico*" class="form-control" required value="{{old('email')}}">
                  </div>
                    <label>Asunto *</label>
                      <input type="text" name="asunto" placeholder="Asunto*" class="form-control" value="{{old('asunto')}}">

                    <div class="section-field textarea">
                    <label>Mensaje *</label>
                     <textarea class="form-control input-message" name="mensaje" placeholder="Mensaje*" rows="7" required ></textarea>
                    </div>
                   <div class="section-field submit-button">
                     <button id="submit" name="submit" type="submit" value="Send" class="button"> Enviar </button>
                   </div>
                  </div>
                {{Form::Close()}}
             </div>
           </div>

        </div>
     </div>
   </div>
 </section>

@endsection


@section('extrajs')
  @if (Session::has('citas'))

      <!-- Pushboar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>{{ (Session::get('citas')) }} Se registro correctamente su formulario de cita ⚡  </h5>
                          <button  data-pushbar-close class="button button-border black x-small">Amper SRL - Eficiencia Energetica </button>
                          </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- end pushbar -->
  @endif

@endsection('extrajs')

    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <title>Dashboard</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="" />
        <meta name="keywords" content="">
        <meta name="author" content="@admin admin" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="theme-color" content="#4680FF">
        <meta name="MobileOptimized" content="width">
        <meta name="HandheldFriendly" content="true">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <link rel="icon" href="{{ asset('favicon.png')}}" type="image/x-icon">

        <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

        @yield('extracss')

        @show

    </head>
    <body class="">

        <div class="loader-bg">
            <div class="loader-track">
                <div class="loader-fill"></div>
            </div>
        </div>

        <nav class="pcoded-navbar menu-light ">
            @include('admin.partials.navigation')
        </nav>

        <header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">

            @include('admin.partials.header')

        </header>


    <div class="pcoded-main-container">
        <div class="pcoded-content">
            @yield('content')
        </div>
    </div>

    <script src="{{ asset('admin/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('admin/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/js/ripple.js') }}"></script>
    <script src="{{ asset('admin/js/pcoded.min.js') }}"></script>

    <script src="{{ asset('admin/js/plugins/apexcharts.min.js') }}"></script>

    <script src="{{ asset('admin/js/pages/dashboard-main.js') }}"></script>

    @yield('extrajs')

    @show


</body>

</html>

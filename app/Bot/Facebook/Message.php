<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;
use \App\Bot\Facebook\Serializer;

class Message
{
    public static function typing($sender)
    {
		$data = ['json' =>
					[
						'recipient' => ['id' => $sender],
						'sender_action' => 'typing_on',
					]
				];
        $response = Serializer::parseResponse($data);

        return $response;
	}

	public static function sendToFbMessenger($sender, $message)
	{
		$data = ['json' =>
					[
						'recipient' => ['id' => $sender],
						'message' => ['text' => $message],
					]
                ];

        $response = Serializer::parseResponse($data);

        return $response;
    }


    public static function sendImage($sender)
    {
        $url = asset('creditos/jessy.jpg');

        // dump($url);

        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'attachment' =>  [
                        'type' => 'image',
                        'payload' => [
                            'url' => $url
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);

        return $response;
    }


}




// function sendGifMessage(recipientId) {
// 	var messageData = {
// 		recipient: {
// 			id: recipientId
// 		},
// 		message: {
// 			attachment: {
// 				type: "image",
// 				payload: {
// 					url: config.SERVER_URL + "/assets/gesam.gif"
// 				}
// 			}
// 		}
// 	};

// 	callSendAPI(messageData);
// }

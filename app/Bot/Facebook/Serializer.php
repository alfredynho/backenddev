<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;

class Serializer
{
    public static function parseResponse($data)
    {
		$client = new \GuzzleHttp\Client;
		$res = $client->request('POST', 'https://graph.facebook.com/v2.6/me/messages?access_token='.env('FB_PAGE_TOKEN'),  $data);

		return $res->getBody();
	}

}

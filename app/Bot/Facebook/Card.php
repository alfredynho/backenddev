<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;
use \App\Blog;
use Illuminate\Support\Facades\DB;
use \App\Bot\Facebook\Serializer;

class Card
{

    public static function itemQuicReplace(){

        // public function operation(): string
        // {
        //     $results = [];
        //     foreach ($this->children as $child) {
        //         $results[] = $child->operation();
        //     }

        //     return "Branch(" . implode("+", $results) . ")";
        // }

        //https://refactoring.guru/design-patterns/composite/php/example


        $results = [];

        $blog = Blog::orderBy('id', 'desc')->take(3)
        ->where('blogs.status','=','1')
        ->get();

        if(20 > 200){
            dump("True");
            foreach ($blog as $bg) {
                $results[] = [
                    'content_type' =>'text',
                    'title' => $bg->title,
                    'payload'=> $bg->author,
                    'image_url' =>'https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png'
                ];
            }
            return $results;
        }else{
            dump("false");

            $default[] = [
                'content_type' =>'text',
                'title' => "sin Registros",
                'payload'=> "START",
                'image_url' =>'https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png'
            ];

            dump($default);

            return $default;
        }

    }



    public static function quickReplace($sender)
    {
        $data = ['json' =>
                    [
                        'recipient' => ['id' => $sender],
                        'message' => [
                            'text' => 'tienes las siguientes opciones!',
                            'quick_replies' => Card::itemQuicReplace()
                        ]
                    ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }



    public static function itemGenericTemplate()
    {
        $results = [];

        $blog = Blog::orderBy('id', 'desc')->take(3)
        ->where('blogs.status','=','1')
        ->get();

        if(200 > 20){
            foreach ($blog as $bg) {
                $results[] = [
                        "title" => $bg->title,
                        "item_url" => "http://myjess.xyz/",
                        "image_url" => "https://myjess.xyz/incos/img/blog.png",
                        "subtitle" => "INCOS LA PAZ.",
                        'buttons' => [
                            [
                                'type' => 'web_url',
                                'url' => 'https://petersfancybrownhats.com',
                                'title' => 'Ver Noticias'
                            ], [
                                'type' => 'postback',
                                'title' => 'Ver Carrera',
                                'payload' => 'DEVELOPER_DEFINED_PAYLOAD'
                            ]
                        ]
                    ];
            }
            return $results;
        }else{

            $default[] = [
                    "title" => "Tarjeta por Defecto",
                    "item_url" => "http://myjess.xyz/",
                    "image_url" => "https://myjess.xyz/incos/img/blog.png",
                    "subtitle" => "INCOS LA PAZ.",
                    'buttons' => [
                        [
                            'type' => 'web_url',
                            'url' => 'https://petersfancybrownhats.com',
                            'title' => 'Ver Noticias'
                        ], [
                            'type' => 'postback',
                            'title' => 'VACIO',
                            'payload' => 'DEVELOPER_DEFINED_PAYLOAD'
                        ]
                    ]
                ];


            return $default;
        }
    }

    public static function genericTemplate($sender)
    {
        $data = ['json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => [
                            'template_type' => 'generic',
                            'elements' => Card::itemGenericTemplate()
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }


    public static function listTemplate($sender)
    {

        $data = ['json' =>
                    [
                    'recipient' => ['id' => $sender],

                    "message" => [
                        "attachment" => [
                            "type" => "template",
                            "payload" => [
                            "template_type" => "list",
                            "top_element_style" => "compact",
                            "elements" => [
                                [
                                "title" => "Classic T-Shirt Collection",
                                "subtitle" => "See all our colors",
                                "image_url" => "https://peterssendreceiveapp.ngrok.io/img/collection.png",
                                "buttons" => [
                                    [
                                    "title" => "View",
                                    "type" => "web_url",
                                    "url" => "https://peterssendreceiveapp.ngrok.io/collection",
                                    "messenger_extensions" => true,
                                    "webview_height_ratio" => "tall",
                                    "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                    ]
                                ]
                                ],
                                [
                                "title" => "Classic White T-Shirt",
                                "subtitle" => "See all our colors",
                                "default_action" => [
                                    "type" => "web_url",
                                    "url" => "https://peterssendreceiveapp.ngrok.io/view?item=100",
                                    "messenger_extensions" => false,
                                    "webview_height_ratio" => "tall"
                                ]
                                ],
                                [
                                "title" => "Classic Blue T-Shirt",
                                "image_url" => "https://peterssendreceiveapp.ngrok.io/img/blue-t-shirt.png",
                                "subtitle" => "100% Cotton, 200% Comfortable",
                                "default_action" => [
                                    "type" => "web_url",
                                    "url" => "https://peterssendreceiveapp.ngrok.io/view?item=101",
                                    "messenger_extensions" => true,
                                    "webview_height_ratio" => "tall",
                                    "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                ],
                                "buttons" => [
                                    [
                                    "title" => "Shop Now",
                                    "type" => "web_url",
                                    "url" => "https://peterssendreceiveapp.ngrok.io/shop?item=101",
                                    "messenger_extensions" => true,
                                    "webview_height_ratio" => "tall",
                                    "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                    ]
                                ]
                                ]
                            ],
                                "buttons" => [
                                [
                                "title" => "View More",
                                "type" => "postback",
                                "payload" => "payload"
                                ]
                            ]
                            ]
                        ]
                        ]
                ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{

    protected $table = 'testimonys';

    protected $fillable = [
        'message',
        'author',
        'status'
    ];
}

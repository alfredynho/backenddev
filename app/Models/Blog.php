<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title',
        'id_category',
        'description',
        'author',
        'image',
        'slug',
        'highlight',
        'status'
    ];
}

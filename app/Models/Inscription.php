<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    protected $table = 'inscriptions';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'item',
        'company',
        'evento',
        'status'
    ];
}

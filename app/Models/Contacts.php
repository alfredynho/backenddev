<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'nombre_c',
        'email_c',
        'celular_c',
        'comentario_c',
        'status_c'
    ];
}

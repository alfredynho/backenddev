<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = 'citas';

    protected $fillable = [
        'nombre',
        'apellidos',
        'email',
        'asunto',
        'mensaje',
        'status'
    ];
}

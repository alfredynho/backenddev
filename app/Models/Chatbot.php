<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chatbot extends Model
{
    protected $fillable = [
        'fb_page_token',
        'hub_verify_token',
        'dialogflow_client',
        'page_id',
        'theme_color_chatbot',
        'logged_in_greeting_chatbot',
        'logged_out_greeting_chatbot',
        'html_fb_template'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'id_address',
        'description',
        'author',
        'image',
        'quota',
        'execute_date',
        'type',
        'slug',
        'active',
        'highlight',
        'status'
    ];
}

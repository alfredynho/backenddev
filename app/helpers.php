 <?php

    use App\Models\Social;

    use Carbon\Carbon;


    //Funcion para generar nombre a imagenes
    function generaCode()
    {
        $uniqid = uniqid();
        $rand_start = rand(1,5);
        $name_image = substr($uniqid,$rand_start,8);

        return $name_image;
    }

    // Function para formatear fecha
    function formaterDate($date)
    {
        $form_date = Carbon::createFromFormat('m-d-Y', $date);
        $format_date = $form_date->format('Y-m-d');

        return $format_date;
    }

    // Function para sacar url de la red social
    function getSocial($name)
    {
        try {
            $url_social = Social::where('name', $name)->firstOrFail();

            if ($url_social->status == 1) {
                return $url_social->url;
            } else {
                return "https://alfredynho.github.io/";
            }

        } catch (Exception $e) {
            return "https://alfredynho.github.io/";
        }

    }


    function numberRand(){
        return uniqid();
    }


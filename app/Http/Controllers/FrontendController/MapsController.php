<?php

namespace App\Http\Controllers\FrontendController;
use App\Models\Maps;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapsController extends Controller
{
    public function index()
    {

        $outletQuery = Maps::query();
        $outletQuery->where('name', 'like', '%'.request('q').'%');
        $outlets = $outletQuery->paginate(25);

        return view('frontend.pages.contactos', compact('mapas'));
    }
}

<?php

namespace App\Http\Controllers\FrontendController;
use \App\Models\Blog;
use \App\Models\Gallery;
use \App\Models\Client;
use \App\Models\Event;
use App\Models\Testimony;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AmperController extends Controller
{
    public function index()
    {
        $gallery = Gallery::where('status',1)->take(6)
        ->orderBy('id','desc')->get();

        $clients = Client::where('status',1)
        ->orderBy('id','desc')->get();

        $testimonie = Testimony::where('status',1)->orderBy('id','desc')->get();

        $events = Event::join('maps','events.id_address','=','maps.id')
        ->select('events.*','maps.name as name_maps')
        ->orderBy('events.id','desc')->paginate(5);

        return view('frontend.index',[
            'gallery' => $gallery,
            'clients' => $clients,
            'testimonie' => $testimonie,
            'events' => $events,
        ]);
    }


    public function amper(){
        $gallery = Gallery::where('status',1)
        ->orderBy('id','desc')->simplePaginate(6);

        return view('frontend.pages.amper',[
            'gallery'=> $gallery,
        ]);
    }

    public function blog(Request $request)
    {
        $blog = Blog::where('blogs.status',1)
        ->simplePaginate(6);

        $_blog = Blog::where('status',1)->where('highlight',1)->get();

        $testimonie = Testimony::where('status',1)->orderBy('id','desc')->get();

        $blog_recent = Blog::orderBy('id', 'desc')->take(3)
        ->where('blogs.status','=','1')
        ->get();

        return view('frontend.pages.blog',[
            'blog'=> $blog,
            '_blog'=> $_blog,
            'testimony'=> $testimonie,
            'blog_recent'=> $blog_recent,
        ]);
    }

    public function detail_blog($slug)
    {
        $meta_property = true;

        $post = Blog::where('slug','=', $slug)->firstOrFail();
        $blog = Blog::where('status',1)->where('highlight',1)->get();

        $testimonie = Testimony::where('status',1)->orderBy('id','desc')->get();

        $blog_recent = Blog::orderBy('id', 'desc')->take(3)
        ->where('blogs.status','=','1')
        ->get();


        return view('frontend.pages.detail_blog',[
            'post' =>$post,
            'meta_property' => $meta_property,
            'blog' =>$blog,
            'testimony' =>$testimonie,
            'blog_recent' => $blog_recent
        ]);
    }

    public function events(Request $request)
    {
        $events = Event::join('maps','events.id_address','=','maps.id')
        ->select('events.*','maps.name as name_maps')
        ->orderBy('events.id','desc')->simplePaginate(6);

        return view('frontend.pages.events',[
            'events'=> $events,
        ]);
    }

    public function detail_event($slug)
    {
        $meta_property = true;

        $event = Event::where('slug','=', $slug)->firstOrFail();
        $events = Event::where('status',1)->where('highlight',1)->get();

        $testimonie = Testimony::where('status',1)->orderBy('id','desc')->get();

        $event_recent = Event::orderBy('id', 'desc')->take(3)
        ->where('events.status','=','1')
        ->get();

        return view('frontend.pages.detail_event',[
            'event' =>$event,
            'meta_property' => $meta_property,
            'events' =>$events,
            'testimony' =>$testimonie,
            'event_recent' =>$event_recent
        ]);
    }

}

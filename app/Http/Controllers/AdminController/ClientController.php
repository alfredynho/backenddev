<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Client;

use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Gallery\GalleryRequest;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::orderBy('id','desc')->get();

        return view('admin.clients.index', [
            'clients' => $clients
        ]);
    }


    public function store(Request $request)
    {
        $client = new Client();

        $client->name = $request->get('name');

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Clients/'.$client->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code.".".$extension);
            \Storage::disk('local')->put('public/Clients/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Clients/".$archivo)." -o ".storage_path("/app/public/Clients/".$webPName));

            $client->image = $webPName;

            Storage::disk('public')->delete('/Clients/'.$archivo);
        }

        $client->status = $request->get('status');
        $client->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.clients.index');
    }


    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        $client->name = $request->input('name');

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Clients/'.$client->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code.".".$extension);
            \Storage::disk('local')->put('public/Clients/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Clients/".$archivo)." -o ".storage_path("/app/public/Clients/".$webPName));

            $client->image = $webPName;

            Storage::disk('public')->delete('/Clients/'.$archivo);
        }

        $client->status = $request->input('status');
        $client->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.clients.index');

    }

	public function destroy($id)
    {
        $_client = Client::findOrFail($id);

        Session::flash('delete','Registro '.$_client->title.' eliminado con Éxito');
        Storage::disk('public')->delete('/Clients/'.$_client->image);

        $_client->delete();

        return Redirect()->route('dashboard.clients.index');
    }
}

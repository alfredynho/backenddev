<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Gallery;

use App\Models\Blog;
use App\Models\Event;
use App\Models\Suscription;
use App\Models\Inscription;


use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        $blog = Blog::orderBy('id','desc')->get();

        $gallery = Gallery::orderBy('id','desc')->get();

        $events = Event::orderBy('id','desc')->get();

        $suscription = Suscription::orderBy('id','desc')->get();

        $inscription = Inscription::orderBy('id','desc')->get();

        $inscripciones = Inscription::join('events','inscriptions.evento','=','events.id')
        ->select('inscriptions.*','events.title as event_name')->take(20)
        ->orderBy('inscriptions.id','desc')->paginate(5);


        return view('admin.dashboard', [
            'blog' => $blog,
            'gallery' => $gallery,
            'events' => $events,
            'suscription' => $suscription,
            'inscription' => $inscription,
            'inscripciones' => $inscripciones,
        ]);



    }
}

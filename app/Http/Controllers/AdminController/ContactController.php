<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Models\Contacts;
use Session;

use App\Http\Controllers\Controller;

use App\Http\Requests\Contact\ContactRequest;


class ContactController extends Controller
{
    public function index()
    {
        return view('frontend.pages.contactos');
    }


    public function indexdashboard()
    {
        $contacts = Contacts::orderBy('id', 'desc')->get();

        return view('admin.contact.index', [
            'contacts' => $contacts
        ]);
    }


    public function store(ContactRequest $request)
    {

        $contact = new Contacts();

        $sw = $request->get('inputHidden');

        $contact->nombre_c = $request->get('nombre_c');
        $contact->celular_c = $request->get('celular_c');
        $contact->email_c = $request->get('email_c');
        $contact->comentario_c = $request->get('comentario_c');

        if ($sw == "1") {
            $contact->status_c = $request->get('status_c');
            $contact->save();

            Session::flash('create', 'Registro creado con Éxito');
            return Redirect()->route('dashboard.contactos.indexdashboard');

        } else {
            $contact->email_c = $request->get('email_c');
            $contact->save();

            Session::flash('contactos', $contact->nombre_c);
            return Redirect()->route('contactos.index');
        }
    }


    public function update(ContactRequest $request, $id)
    {
        $contact = Contacts::findOrFail($id);

        $contact->nombre_c = $request->input('nombre_c');
        $contact->celular_c = $request->input('celular_c');
        $contact->email_c = $request->input('email_c');
        $contact->comentario_c = $request->input('comentario_c');

        $contact->status_c = $request->input('status_c');
        $contact->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.contactos.indexdashboard');
    }

    public function destroy($id)
    {
        $contact = Contacts::findOrFail($id);

        Session::flash('delete', 'Registro ' . $contact->nombre . ' eliminado con Éxito');

        $contact->delete();

        return Redirect()->route('dashboard.contactos.indexdashboard');
    }

}

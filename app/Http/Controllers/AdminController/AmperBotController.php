<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AmperBotController extends Controller
{
    public function index()
    {
        // $blog = Blog::join('categoryblogs','blogs.id_category','=','categoryblogs.id')
        // ->select('blogs.*','categoryblogs.title as category_title')
        // ->orderBy('blogs.id','desc')->get();

        // $categorys = CategoryBlog::where('status',1)->get();

        $amperbot ="Amperbot";

        return view('admin.amperbot.index', [
            'amperbot' => $amperbot,
        ]);
    }
}

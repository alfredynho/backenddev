<?php

namespace App\Http\Controllers\AdminController;
use App\Models\Maps;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use App\Models\Inscription;
use App\Models\Event;


class InscriptionController extends Controller
{
    public function index()
    {
        $inscripciones = Inscription::join('events','inscriptions.evento','=','events.id')
        ->select('inscriptions.*','events.title as event_name')
        ->orderBy('inscriptions.id','desc')->paginate(5);

        $maps = Maps::orderBy('id','desc')->get();

        $events = Event::join('maps','events.id_address','=','maps.id')
        ->select('events.*','maps.name as name_maps')
        ->orderBy('events.id','desc')->paginate(5);


        return view('admin.inscripciones.index', [
            'inscripciones' => $inscripciones,
            'maps' => $maps,
            'events' => $events,
        ]);
    }

    public function store(Request $request)
    {
        $inscription = new Inscription();

        $sw=$request->get('inputHidden');

        if($sw=="1"){

            $inscription->first_name = $request->input('firstname');
            $inscription->last_name = $request->input('lastname');
            $inscription->email = $request->input('email');
            $inscription->company = $request->input('compania');
            $inscription->item = $request->input('rubro');
            $inscription->evento = $request->input('evento');
            $inscription->status = 1;
            $inscription->save();

            Session::flash('create','Registro creado con Éxito');
            return Redirect()->route('dashboard.inscripcion.index');

        }else{
            $inscription->first_name = $request->input('firstname');
            $inscription->last_name = $request->input('lastname');
            $inscription->email = $request->input('email');
            $inscription->company = $request->input('compania');
            $inscription->item = $request->input('rubro');
            $inscription->evento = $request->input('evento');

            $inscription->status = 1;
            $inscription->save();

            Session::flash('inscription',$inscription->first_name. '  '.$inscription->last_name);
            return Redirect()->route('index');
        }


    }


    public function update(Request $request, $id)
    {
        $inscription = Inscription::findOrFail($id);

        $sw=$request->get('inputHidden');

        if($sw=="1"){

            $inscription->first_name = $request->get('firstname');
            $inscription->last_name = $request->get('lastname');
            $inscription->email = $request->get('email');
            $inscription->company = $request->get('compania');
            $inscription->item = $request->get('rubro');
            $inscription->evento = $request->get('evento');
            $inscription->status = $request->get('status');
            $inscription->save();

            Session::flash('update','Registro creado con Éxito');
            return Redirect()->route('dashboard.inscripcion.index');

        }else{
            $inscription->first_name = $request->get('firstname');
            $inscription->last_name = $request->get('lastname');
            $inscription->email = $request->get('email');
            $inscription->company = $request->get('compania');
            $inscription->item = $request->get('rubro');
            $inscription->evento = $request->get('evento');

            $inscription->status = 1;
            $inscription->save();

            Session::flash('inscription',$inscription->first_name. '  '.$inscription->last_name);
            return Redirect()->route('index');
        }
    }

	public function destroy($id)
    {
        $inscription = Inscription::findOrFail($id);

        Session::flash('delete','Registro '.$inscription->first_name.' eliminado con Éxito');

        $inscription->delete();

        return Redirect()->route('dashboard.inscripcion.index');
    }


}

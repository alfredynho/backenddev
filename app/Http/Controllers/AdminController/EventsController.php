<?php

namespace App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Event\EventRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use Session;

use App\Models\Event;
use App\Models\Maps;

class EventsController extends Controller
{
    public function index()
    {
        $events = Event::join('maps','events.id_address','=','maps.id')
        ->select('events.*','maps.name as name_maps')
        ->orderBy('events.id','desc')->paginate(5);

        $maps = Maps::all();

        return view('admin.eventos.index', [
            'events' => $events,
            'maps' => $maps,
        ]);
    }

    public function store(EventRequest $request)
    {
        $event = new Event();

        $event->title = $request->get('title');
        $event->description = $request->get('description');
        $event->highlight = $request->get('highlight');
        $event->quota = 0;
        $event->id_address = $request->get('address');

        $_imgType = $event->type = $request->get('type_event');

        $event->author = auth()->user()->id;

        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $event->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);
        $event->execute_date = date('Y-m-d H:i:s');

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($_imgType).$code.".".$extension;
            \Storage::disk('local')->put('public/Events/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Events/".$archivo)." -o ".storage_path("/app/public/Events/".$webPName));

            $event->image = $webPName;

            Storage::disk('public')->delete('/Events/'.$archivo);

        }

        $event->status = $request->get('status');
        $event->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.events.index');
    }


    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);

        $event->title = $request->input('title');
        $event->description = $request->input('description');
        $event->highlight = $request->input('highlight');
        $event->id_address = $request->input('address');
        $event->quota = 0;

        $_imgType = $event->type = $request->input('type_event');

        $event->author = auth()->user()->id;

        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $event->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Blog/'.$event->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($_imgType).$code.".".$extension;
            \Storage::disk('local')->put('public/Events/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Events/".$archivo)." -o ".storage_path("/app/public/Events/".$webPName));

            $event->image = $webPName;

            Storage::disk('public')->delete('/Events/'.$archivo);


        }

        $event->status = $request->input('status');
        $event->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.events.index');
    }


	public function destroy($id)
    {
        $_event = Event::findOrFail($id);

        Session::flash('delete','Registro '.$_event->title.' eliminado con Éxito');
        Storage::disk('public')->delete('/Events/'.$_event->image);

        $_event->delete();

        return Redirect()->route('dashboard.events.index');
    }

}

<?php

namespace App\Http\Controllers\AdminController;
use App\Models\Testimony;

use Illuminate\Http\Request;
use App\Http\Requests\Testimonie\TestimonieRequest;
use App\Http\Controllers\Controller;
use Session;


class TestimonyController extends Controller
{
    public function index()
    {
        $testimonie = Testimony::orderBy('id','desc')->get();

        return view('admin.testimonie.index', [
            'testimonie' => $testimonie
        ]);
    }

    public function store(Request $request)
    {

        $testimonie = new Testimony();

        $testimonie->message = $request->get('message');
        $testimonie->author = $request->get('author');
        $testimonie->status = $request->get('status');
        $testimonie->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.testimony.index');
    }


    public function update(Request $request, $id)
    {
        $testimonie = Testimony::findOrFail($id);

        $testimonie->message = $request->input('message');
        $testimonie->author = $request->input('author');
        $testimonie->status = $request->input('status');
        $testimonie->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.testimony.index');
    }

	public function destroy($id)
    {
        $testimonie = Testimony::findOrFail($id);

        Session::flash('delete','Registro '.$testimonie->author.' eliminado con Éxito');

        $testimonie->delete();

        return Redirect()->route('dashboard.testimony.index');
    }
}

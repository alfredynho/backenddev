<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Social;

use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialController extends Controller
{
    public function index()
    {
        $socials = Social::orderBy('id','desc')->get();

        return view('admin.social.index', [
            'socials' => $socials
        ]);
    }

    public function update(Request $request, $id)
    {
        $socials = Social::findOrFail($id);

        $socials->name = $request->input('name');
        $socials->url = $request->input('url');

        $socials->status = $request->input('status');
        $socials->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.social.index');

    }
}

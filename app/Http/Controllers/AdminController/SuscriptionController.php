<?php

namespace App\Http\Controllers\AdminController;
use App\Models\Suscription;

use App\Http\Requests\Suscription\SuscriptionRequest;
use App\Http\Controllers\Controller;
use Session;

class SuscriptionController extends Controller
{
    public function index()
    {
        $suscription = Suscription::orderBy('id','desc')->get();

        return view('admin.suscription.index', [
            'suscription' => $suscription
        ]);
    }

    public function store(SuscriptionRequest $request)
    {
        $suscription = new Suscription();

        $sw=$request->get('inputHidden');

        if($sw=="1"){

            $suscription->email = $request->get('email');
            $suscription->status = $request->get('status');
            $suscription->save();

            Session::flash('create','Registro creado con Éxito');
            return Redirect()->route('dashboard.suscription.index');

        }else{
            $suscription->email = $request->get('email');
            $suscription->save();

            Session::flash('suscription',$suscription->email);
            return Redirect()->route('index');
        }

    }


    public function update(SuscriptionRequest $request, $id)
    {
        $suscription = Suscription::findOrFail($id);

        $suscription->email = $request->input('email');
        $suscription->status = $request->input('status');
        $suscription->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.suscription.index');
    }

	public function destroy($id)
    {
        $suscription = Suscription::findOrFail($id);

        Session::flash('delete','Registro '.$suscription->email.' eliminado con Éxito');

        $suscription->delete();

        return Redirect()->route('dashboard.suscription.index');
    }

}

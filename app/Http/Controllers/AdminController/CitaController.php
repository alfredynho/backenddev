<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use App\Models\Cita;
use App\Http\Requests\Cita\CitaRequest;


class CitaController extends Controller
{
    public function index()
    {
        return view('frontend.pages.citas');
    }

    public function indexdashboard()
    {
        $citas = Cita::orderBy('id','desc')->get();

        return view('admin.cita.index', [
            'citas' => $citas
        ]);
    }


    public function store(CitaRequest $request)
    {
        $citas = new Cita();

        $sw = $request->get('inputHidden');

        $citas->nombre = $request->get('nombre');
        $citas->apellidos = $request->get('apellidos');
        $citas->email = $request->get('email');
        $citas->asunto = $request->get('asunto');
        $citas->mensaje = $request->get('mensaje');

        if ($sw == "1") {
            $citas->status = $request->get('status');
            $citas->save();

            Session::flash('create', $citas->nombre . '  ' . $citas->apellidos);
            return Redirect()->route('dashboard.citas.indexdashboard');

        }else{
            $citas->status = 1;
            $citas->save();

            Session::flash('citas', $citas->nombre . '  ' . $citas->apellidos);
            return Redirect()->route('citas.index');
        }
    }

    public function update(CitaRequest $request, $id)
    {
        $citas = Cita::findOrFail($id);

        $citas->nombre = $request->input('nombre');
        $citas->apellidos = $request->input('apellidos');
        $citas->email = $request->input('email');
        $citas->asunto = $request->input('asunto');
        $citas->mensaje = $request->input('mensaje');

        $citas->status = $request->input('status');
        $citas->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.citas.indexdashboard');
    }

    public function destroy($id)
    {
        $cita = cita::findOrFail($id);

        Session::flash('delete', 'Registro ' . $cita->email . $cita->nombre. $cita->apellidos .' eliminado con Éxito');

        $cita->delete();

        return Redirect()->route('dashboard.citas.indexdashboard');
    }

}

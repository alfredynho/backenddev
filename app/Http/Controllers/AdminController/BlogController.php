<?php

namespace App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogRequest;
use Illuminate\Support\Facades\Storage;

use Session;

use App\Models\Blog;


class BlogController extends Controller
{
    public function index()
    {

        $blog = Blog::orderBy('blogs.id','desc')->get();

        return view('admin.blog.index', [
            'blog' => $blog,
        ]);
    }

    public function store(BlogRequest $request)
    {
        $blog = new Blog();

        $blog->title = $request->get('title');
        $blog->description = $request->get('description');
        $blog->highlight = $request->get('highlight');

        $_imgCategory = $blog->category = $request->get('category');

        $blog->author = auth()->user()->id;
        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $blog->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($_imgCategory).$code.".".$extension;
            \Storage::disk('local')->put('public/Blog/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Gallery/".$archivo)." -o ".storage_path("/app/public/Gallery/".$webPName));

            $blog->image = $webPName;

            Storage::disk('public')->delete('/Gallery/'.$archivo);
        }

        $blog->status = $request->get('status');
        $blog->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.blog.index');

    }


    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::findOrFail($id);

        $blog->title = $request->input('title');
        $blog->description = $request->input('description');
        $blog->highlight = $request->input('highlight');

        $_imgCategory = $blog->category = $request->input('category');

        $blog->author = auth()->user()->id;

        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $blog->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Blog/'.$blog->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($_imgCategory).$code.".".$extension;
            \Storage::disk('local')->put('public/Blog/'.$archivo, \File::get($img));

            $blog->image = $archivo;
        }

        $blog->status = $request->input('status');
        $blog->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.blog.index');

    }


	public function destroy($id)
    {
        $_blog = Blog::findOrFail($id);

        Session::flash('delete','Registro '.$_blog->title.' eliminado con Éxito');
        Storage::disk('public')->delete('/Blog/'.$_blog->image);

        $_blog->delete();

        return Redirect()->route('dashboard.blog.index');

    }

}

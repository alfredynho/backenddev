<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Gallery;

use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Gallery\GalleryRequest;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function index()
    {
        $gallery = Gallery::orderBy('id','desc')->get();

        return view('admin.gallery.index', [
            'gallery' => $gallery
        ]);
    }


    public function store(Request $request)
    {
        $gallery = new Gallery();

        $gallery->title = $request->get('title');

        $img = $request->file('image');


        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Gallery/'.$gallery->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code.".".$extension);
            \Storage::disk('local')->put('public/Gallery/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Gallery/".$archivo)." -o ".storage_path("/app/public/Gallery/".$webPName));

            $gallery->image = $webPName;

            Storage::disk('public')->delete('/Gallery/'.$archivo);

        }

        $gallery->status = $request->get('status');
        $gallery->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.gallery.index');
    }


    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($id);

        $gallery->title = $request->input('title');

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Gallery/'.$gallery->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code.".".$extension);
            \Storage::disk('local')->put('public/Gallery/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Gallery/".$archivo)." -o ".storage_path("/app/public/Gallery/".$webPName));

            $gallery->image = $webPName;

            Storage::disk('public')->delete('/Gallery/'.$archivo);


        }

        $gallery->status = $request->input('status');
        $gallery->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.gallery.index');

    }

	public function destroy($id)
    {
        $_gallery = Gallery::findOrFail($id);

        Session::flash('delete','Registro '.$_gallery->title.' eliminado con Éxito');
        Storage::disk('public')->delete('/Gallery/'.$_gallery->image);

        $_gallery->delete();

        return Redirect()->route('dashboard.gallery.index');
    }
}

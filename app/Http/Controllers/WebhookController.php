<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Action;
use \App\Question;
use \App\Messenger;
use \App\Bot\Facebook\Message;
use \App\Bot\Facebook\Card;
use \App\Bot\Facebook\User;
use \App\Bot\Facebook\Button;


use DialogFlow\Client;

class WebhookController extends Controller
{
    public function getWebhook(Request $request)
    {
        if ($request->get('hub_mode') == 'subscribe' and $request->get('hub_verify_token') === env('HUB_VERIFY_TOKEN')) {
            return response($request->get('hub_challenge'));
        }
        return response('Error, verify token doesn\'t match', 400);
    }

    public function postWebhook(Request $request)
    {

        $content = json_decode($request->getContent(), true);

        $jess = $content['entry'][0]['messaging'] ? $content['entry'][0]['messaging'] : null;
        $sender = $jess[0]['sender']['id'];

        if (isset($jess[0]['message']))
        {
            //dump("message");
        }
        elseif(isset($jess[0]['postback']))
        {
            //dump("postback");
            $has_postback = $jess[0]['postback']['payload'];
            //dump("El POSTBACK " . $has_postback);

            if($has_postback == 'BOT_INCOS_START')
            {
                $response = Message::typing($sender);
                $_user = User::getUser($sender);
                $response = Message::sendToFbMessenger(
                    $sender,
                    'Hola 👋 😀 , mi nombre es cloudbot y sere tu asistente virtual 🎓 ...');
            }

            if($has_postback == 'CREDITOS_BOT')
            {
                $response = Message::typing($sender);
                $response = Message::sendToFbMessenger($sender, "Creditos @alfredynho 😀..");

            }

            if ($has_postback == 'BOT_AYUDA') {
                $response = Message::typing($sender);
                $response = Message::sendToFbMessenger($sender, "Botar un menu de ayuda");
            }


        }
        else
        {
            // dump("otro");
        }

        $postArray = isset($content['entry'][0]['messaging']) ? $content['entry'][0]['messaging'] : null;

        $response = [];
        $has_message = false;
        $is_echo = true;

        if (!is_null($postArray)) {
            $sender = $postArray[0]['sender']['id'];
            $has_message = isset($postArray[0]['message']['text']);

            $is_echo = isset($postArray[0]['message']['is_echo']);
        }
        if ($has_message && !$is_echo) {

            $reply = $postArray[0]['message']['text'];

            // $actions = Action::where('actions.status','1')
            // ->select('actions.*')
            // ->get();

            // 	$array_actions = array();

            // 	foreach($actions as $action)
            // 	{
            // 			$vv = $action->id;
            // 			$key = $action->name;
            // 			$array_actions[(string)$vv] = $key;
            // 	}


            try {
                $client = new Client(env('DIALOGFLOW_CLIENT'));

                $query = $client->get('query', [
                    'query' => $reply,
                    'sessionId' => time(),
                ]);

                $response = json_decode((string) $query->getBody(), true);

                // dump($response);
                $speech = $response['result']['fulfillment']['speech'] ?: json_encode($response['result']['fulfillment']['messages'][0]['payload']);

                $_action = $response['result']['action'];
            } catch (\Exception $error) {
                error_log("un error");
            }

            if (1 > 110) {
                foreach ($array_actions as $key => $value) {
                    if ($value == $_action) {
                        $dialog = $this->searchQuestions($key);
                        $response = $this->typing($sender);
                        $response = $this->sendToFbMessenger($sender, $dialog);
                    }
                }
            } else {

                try {

                    $_user = User::getUser($sender);
                    $response = Message::typing($sender);
                    $response = Message::sendToFbMessenger($sender, $speech);
                    // $response = Message::sendImage($sender);
                    // $response = Message::sendToFbMessenger($sender, 'tu Nombre completo es ' . $_user['first_name'] . ' ' . $_user['last_name']);
                    // $response = Button::templateButton($sender);
                    // $response = Card::genericTemplate($sender);

                } catch (\Exception $error) {
                    error_log("error en usuarios/o mensajes");
                }

                // $response = Card::quickReplace($sender);
                // $response = Card::genericTemplate($sender);
                // $response = $this->genericTemplate($sender);
                // $response = $this->quickReplace($sender);
            }
        }
        return response($response, 200);
    }


    private function searchQuestions($key)
    {
        $questions = Question::where('questions.status', '1')
            ->select('questions.*')
            ->get();

        $array_questions = array();

        foreach ($questions as $question) {
            $key = $question->id;
            $value = $question->answer;

            $array_questions[(string) $key] = $value;
        }

        $responses = "";
        foreach ($array_questions as $_key => $_value) {
            if ($_key == $key) {
                $responses = $_value;
            }
        }

        return $responses;
    }
}

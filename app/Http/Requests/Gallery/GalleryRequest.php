<?php

namespace App\Http\Requests\Gallery;

use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    public function messages()
    {
        return [
            'title' => 'El titulo es requerido',
            'title.unique' => 'El titulo ingresado no puede ser usado ya esta registrado!',
            'title.min' => 'El titulo es muy corto se debe tener al menos 6 dígitos',
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required|min:6|unique:gallerys',
        ];
    }
}

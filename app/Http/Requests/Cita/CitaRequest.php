<?php

namespace App\Http\Requests\Cita;

use Illuminate\Foundation\Http\FormRequest;

class CitaRequest extends FormRequest
{
    public function messages()
    {
        return [
            'nombre' => 'El nombre es requerido',
            'apellidos' => 'Los apellidos son requeridos',
            'email' => "El email es requerido",
            'asunto' => "El asunto es requerido",
            'mensaje' => "El mensaje es requerido",
            'nombre.min' => 'El nombre es muy corto se debe tener al menos 3 caracteres',
            'apellidos.min'=>'Los apellidos son muy cortos, debe tener por lo menos 3 caracteres',
            'asunto.min'=>'El campo asunto debe tener por lo menos 3 caracteres',
            'mensaje.min'=>'El campo mensaje debe tener por lo menos 3 caracteres',
        ];
    }

    public function rules()
    {
        return [
            'nombre' => 'required|min:3',
            'apellidos' => 'required|min:3',
            'email' => 'required',
            'asunto' => 'required|min:3',
            'mensaje' => 'required|min:3',
        ];
    }
}

<?php

namespace App\Http\Requests\Suscription;

use Illuminate\Foundation\Http\FormRequest;

class SuscriptionRequest extends FormRequest
{
    public function messages()
    {
        return [
            'email' => 'El email es requerido',
            'email.unique' => 'El email ingresado ya se encuentra registrado',
            'id.unique' => 'El registro ya esta hecho',
        ];
    }

    public function rules()
    {
        return [
            'email'=>'required|email|unique:suscriptions,email,'.$this->id,
        ];
    }
}

<?php

namespace App\Http\Requests\Testimonie;

use Illuminate\Foundation\Http\FormRequest;

class TestimonieRequest extends FormRequest
{
    public function messages()
    {
        return [
            'author' => 'El autor(a) es requerido',
            'author.unique' => 'El autor(a) ingresado no puede ser usado ya esta registrado!',
            'author.min' => 'El titulo es muy corto se debe tener al menos 6 dígitos',
            'message.min'=>'Descripción muy corta, debe tener por lo menos 6 digitos',
        ];
    }

    public function rules()
    {
        return [
            'author' => 'required|min:6|unique:testimonies',
            'message' => 'required|min:6',
        ];
    }
}

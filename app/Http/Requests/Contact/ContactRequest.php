<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{

    public function messages()
    {
        return [
            'nombre_c' => 'El nombre es requerido',
            'celular_c' => 'El número de celular requerido',
            'email_c' => "El email es requerido",
            'asunto_c' => "El asunto es requerido",
            'comentario_c' => "El comentario es requerido",
            'nombre_c.min' => 'El nombre es muy corto se debe tener al menos 3 caracteres',
            'celular_c.min' => 'El número es muy cortos, debe tener por lo menos 3 caracteres',
            'comentario_c.min' => 'El campo comentario debe tener por lo menos 3 caracteres',
        ];
    }

    public function rules()
    {
        return [
            'nombre_c' => 'required|min:3',
            'celular_c' => 'required|min:3',
            'email_c' => 'required',
            'comentario_c' => 'required|min:3',
        ];
    }
}

<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{

    public function messages()
    {
        return [
            'title' => 'El título es requerido',
            'title.unique' => 'El título ingresado no puede ser usado ya esta registrado!',
            'title.min' => 'El titulo es muy corto se debe tener al menos 6 caracteres',
            'description.min'=>'Descripción muy corta, debe tener por lo menos 6 digitos',
            'image.max' => "El tamaño máximo de archivo para cargar es de 5 MB (5120 KB). Si está subiendo una imagen, intente reducir su resolución para que sea inferior a 5 MB",
            'id.unique' => 'El registro ya esta hecho',
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:blogs,title,'.$this->id,
            'description' => 'required|min:6',
            'image' => 'image|mimes:jpeg,png,jpg|max:5120',
            'slug'  => 'unique:blogs'
        ];
    }
}

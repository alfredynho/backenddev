<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title' => 'El título es requerido',
            'address' => 'La direccion es requerida',
            'title.unique' => 'El título ingresado no puede ser usado ya esta registrado!',
            'title.min' => 'El titulo es muy corto se debe tener al menos 6 caracteres',
            'description.min'=>'Descripción muy corta, debe tener por lo menos 6 digitos',
            'image.max' => "El tamaño máximo de archivo para cargar es de 5 MB (5120 KB). Si está subiendo una imagen, intente reducir su resolución para que sea inferior a 5 MB"
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required|title|min:6|unique:events,title,'.$this->id,
            'description' => 'required|min:6',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:5120',
            'slug'  => 'unique:events',
            'address' => 'required',
        ];
    }
}

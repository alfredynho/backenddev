<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_address')->unsigned();
            $table->string('title', 255);
            $table->longText('description');
            $table->string('author', 20);
            $table->string('image', 255)->nullable();
            $table->integer('quota'); // Cupo
            $table->dateTime('execute_date');	// Fecha del Evento
            $table->string('type',100); // Presencial , Virtual
            $table->string('slug')->index()->unique();
            $table->boolean('highlight')->default(0);
            $table->boolean('active')->default(1);
            $table->boolean('status', 150); // Inscripcion, cerrado , llenado
            $table->timestamps();

            $table->foreign('id_address')->references('id')->on('maps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

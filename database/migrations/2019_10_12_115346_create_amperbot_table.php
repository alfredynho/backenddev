<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmperbotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amperbot', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fb_page_token', 255)->nullable();
            $table->string('hub_verify_token', 50)->nullable();
            $table->string('dialogflow_client', 50)->nullable();
            $table->string('page_id', 50)->nullable();
            $table->string('theme_color_chatbot', 50)->nullable();
            $table->string('logged_in_greeting_chatbot', 50)->nullable();
            $table->string('logged_out_greeting_chatbot', 50)->nullable();
            $table->longtext('html_fb_template')->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amperbot');
    }
}

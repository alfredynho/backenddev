<?php

// <<<<<   URL FRONTEND  >>>>>> //
Route::get('/','FrontendController\AmperController@index')->name('index');

Route::get('/eventos','FrontendController\AmperController@events')->name('eventos');
Route::get('/evento/{slug}','FrontendController\AmperController@detail_event')->name('detail_event');


Route::get('/servicios', function () {
    return view('frontend.pages.services');
})->name('services');


Route::get('/soluciones', function () {
    return view('frontend.pages.solutions');
})->name('solutions');


Route::get('/blog','FrontendController\AmperController@blog')->name('blog');
Route::get('/blog-amper/{slug}','FrontendController\AmperController@detail_blog')->name('detail_blog');

Route::get('/amper','FrontendController\AmperController@amper')->name('amper');

Route::get('/webhook', 'WebhookController@getWebhook')->name('getbot');
Route::post('/webhook', 'WebhookController@postWebhook')->name('postbot');

Route::post('/suscripcion', 'AdminController\SuscriptionController@store')->name('dashboard.suscription.store');


// Servicios API para consumir los mapas
Route::resource('maps', 'FrontendController\MapsController');

// <<<<<   URL FRONTEND  >>>>>> //


// <<<<<   URL BACKEND  >>>>>> //

Auth::routes();

Route::get('/dashboard', 'AdminController\DashboardController@index')->name('dashboard.dashboard.index')->middleware('auth');


// URLS PARA GESTION DE BLOG DE AMPER
Route::get('/dashboard/blog', 'AdminController\BlogController@index')->name('dashboard.blog.index')->middleware('auth');
Route::delete('/dashboard/blog/{id}', 'AdminController\BlogController@destroy')->name('dashboard.blog.destroy')->middleware('auth');
Route::put('/dashboard/blog/{id}','AdminController\BlogController@update')->name('dashboard.blog.update')->middleware('auth');
Route::post('/dashboard/blog', 'AdminController\BlogController@store')->name('dashboard.blog.store')->middleware('auth');


// URLS PARA MAPAS (UBICACIONES AMPER LATITUD LONGITUD)
Route::get('/dashboard/mapas', 'AdminController\MapsController@index')->name('dashboard.mapas.index')->middleware('auth');
Route::delete('/dashboard/mapas/{id}', 'AdminController\MapsController@destroy')->name('dashboard.mapas.destroy')->middleware('auth');
Route::put('/dashboard/mapas/{id}','AdminController\MapsController@update')->name('dashboard.mapas.update')->middleware('auth');
Route::post('/dashboard/mapas', 'AdminController\MapsController@store')->name('dashboard.mapas.store')->middleware('auth');


// URLS PARA CRUD EVENTOS
Route::get('/dashboard/eventos', 'AdminController\EventsController@index')->name('dashboard.events.index')->middleware('auth');
Route::delete('/dashboard/eventos/{id}', 'AdminController\EventsController@destroy')->name('dashboard.events.destroy')->middleware('auth');
Route::put('/dashboard/eventos/{id}','AdminController\EventsController@update')->name('dashboard.events.update')->middleware('auth');
Route::post('/dashboard/eventos', 'AdminController\EventsController@store')->name('dashboard.events.store')->middleware('auth');


// URLS PARA TESTIMONIOS DE ASISTENTES A LOS EVENTOS
Route::get('/dashboard/testimonios', 'AdminController\TestimonyController@index')->name('dashboard.testimony.index')->middleware('auth');
Route::delete('/dashboard/testimonios/{id}', 'AdminController\TestimonyController@destroy')->name('dashboard.testimony.destroy')->middleware('auth');
Route::put('/dashboard/testimonios/{id}','AdminController\TestimonyController@update')->name('dashboard.testimony.update')->middleware('auth');
Route::post('/dashboard/testimonios', 'AdminController\TestimonyController@store')->name('dashboard.testimony.store')->middleware('auth');

// URLS PARA GALERIA DE IMAGENES DE LOS EVENTOS AMPER
Route::get('/dashboard/galeria', 'AdminController\GalleryController@index')->name('dashboard.gallery.index')->middleware('auth');
Route::delete('/dashboard/galeria/{id}', 'AdminController\GalleryController@destroy')->name('dashboard.gallery.destroy')->middleware('auth');
Route::put('/dashboard/galeria/{id}','AdminController\GalleryController@update')->name('dashboard.gallery.update')->middleware('auth');
Route::post('/dashboard/galeria', 'AdminController\GalleryController@store')->name('dashboard.gallery.store')->middleware('auth');

// URLS PARA AGREGAR IMAGENES DE CLIENTES
Route::get('/dashboard/clientes', 'AdminController\ClientController@index')->name('dashboard.clients.index')->middleware('auth');
Route::delete('/dashboard/clientes/{id}', 'AdminController\ClientController@destroy')->name('dashboard.clients.destroy')->middleware('auth');
Route::put('/dashboard/clientes/{id}','AdminController\ClientController@update')->name('dashboard.clients.update')->middleware('auth');
Route::post('/dashboard/clientes', 'AdminController\ClientController@store')->name('dashboard.clients.store')->middleware('auth');


// URLS PARA ADMINISTRACION DE REDES SOCIALES
Route::get('/dashboard/redes', 'AdminController\SocialController@index')->name('dashboard.social.index')->middleware('auth');
Route::delete('/dashboard/redes/{id}', 'AdminController\SocialController@destroy')->name('dashboard.social.destroy')->middleware('auth');
Route::put('/dashboard/redes/{id}','AdminController\SocialController@update')->name('dashboard.social.update')->middleware('auth');
Route::post('/dashboard/redes', 'AdminController\SocialController@store')->name('dashboard.social.store')->middleware('auth');


// URLS PARA ADMINISTRACION DE INSCRIPCIONES A EVENTOS
Route::get('/dashboard/inscripciones', 'AdminController\InscriptionController@index')->name('dashboard.inscripcion.index')->middleware('auth');
Route::delete('/dashboard/inscripciones/{id}', 'AdminController\InscriptionController@destroy')->name('dashboard.inscripcion.destroy')->middleware('auth');
Route::put('/dashboard/inscripciones/{id}','AdminController\InscriptionController@update')->name('dashboard.inscripcion.update')->middleware('auth');


Route::post('/inscripcion', 'AdminController\InscriptionController@store')->name('dashboard.inscription.store');


// URLS PARA CITAS TANTO EN FRONTEND COMO EN BACKEND
Route::post('/citas', 'AdminController\CitaController@store')->name('dashboard.citas.store');

Route::get('/dashboard/citas', 'AdminController\CitaController@indexdashboard')->name('dashboard.citas.indexdashboard');
Route::delete('/dashboard/cita/{id}', 'AdminController\CitaController@destroy')->name('dashboard.citas.destroy');
Route::put('/dashboard/cita/{id}', 'AdminController\CitaController@update')->name('dashboard.citas.update');
Route::post('/dashboard/cita', 'AdminController\CitaController@store')->name('dashboard.citas.store');

// url para frontend
Route::get('/programar-citas', 'AdminController\CitaController@index')->name('citas.index');



// URLS PARA CONTACTOS
Route::post('/contactos', 'AdminController\ContactController@store')->name('dashboard.contactos.store');

Route::get('/dashboard/contactos', 'AdminController\ContactController@indexdashboard')->name('dashboard.contactos.indexdashboard');
Route::delete('/dashboard/contacto/{id}', 'AdminController\ContactController@destroy')->name('dashboard.contactos.destroy');
Route::put('/dashboard/contacto/{id}', 'AdminController\ContactController@update')->name('dashboard.contactos.update');
Route::post('/dashboard/contacto', 'AdminController\ContactController@store')->name('dashboard.contactos.store');

// url para frontend
Route::get('/contactos-amper', 'AdminController\ContactController@index')->name('contactos.index');


Route::get('/dashboard/suscripcion', 'AdminController\SuscriptionController@index')->name('dashboard.suscription.index')->middleware('auth');
Route::delete('/dashboard/suscripcion/{id}', 'AdminController\SuscriptionController@destroy')->name('dashboard.suscription.destroy')->middleware('auth');
Route::put('/dashboard/suscripcion/{id}','AdminController\SuscriptionController@update')->name('dashboard.suscription.update')->middleware('auth');


// <<<<<   URL BACKEND  >>>>>> //


// <<<<<   URLS CHATBOT  >>>>>> //

// URLS PARA ADMINISTRACION DE ASISTENTE VIRTUAL
Route::get('/dashboard/amperbot', 'AdminController\AmperBotController@index')->name('dashboard.amperbot.index')->middleware('auth');
Route::delete('/dashboard/amperbot/{id}', 'AdminController\AmperBotController@destroy')->name('dashboard.amperbot.destroy')->middleware('auth');
Route::put('/dashboard/amperbot/{id}','AdminController\AmperBotController@update')->name('dashboard.amperbot.update')->middleware('auth');
Route::post('/dashboard/amperbot', 'AdminController\AmperBotController@store')->name('dashboard.amperbot.store')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
